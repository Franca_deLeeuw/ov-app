**OVAPP**

OVApp is an application that can be used to plan a journey using public transport.

To use this application you must have the latest version of java<br> 
installed on your machine. It is not recommended to use the application on a<br>
small screen.

This application is made for a school project<br> 
as part of the study "AD Software Development" at the Hogeschool Utrecht.<br>
As such it is not meant for public use, or perfect operation.

The goal of this application is to learn a lot, impress our teachers and peers,<br>
and get a 10/10. Boom.

The contributers to this project are:
* Abdurabi
* Franca
* Jan
* Kevin
* Rico
* Ruben

Any questions regarding this project can be sent to:
ruben.dekoning@student.hu.nl