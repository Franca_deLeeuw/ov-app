package trein;
import java.lang.reflect.Array;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;

public class TreinTijd
{

	private String stationVertrek;
	private String stationAankomst;
	private String plaatsNaam;
	private String perron;
	private String traject;
	private String reisTijd;
	private String reisAfstand;
	private  String treinNummer;
	private String tussenStop;
	private String typeTrein;
	private String eindPunt;
	public ArrayList<LocalTime> reisTijden;

	public TreinTijd( String stationVertrek, String stationAankomst, String plaatsNaam, String perron, String traject,
					  String reisTijd, String reisAfstand, String treinNummer, String tussenStop, String typeTrein,
					  String eindPunt, ArrayList reisTijden)
	{
		this.stationVertrek = stationVertrek;
		this.stationAankomst = stationAankomst;
		this.plaatsNaam = plaatsNaam;
		this.perron = perron;
		this.traject = traject;
		this.reisTijd = reisTijd;
		this.reisAfstand = reisAfstand;
		this.treinNummer = treinNummer;
		this.tussenStop = tussenStop;
		this.typeTrein = typeTrein;
		this.eindPunt = eindPunt;
		this.reisTijden = reisTijden;
	}

	public String getStationVertrek()
	{
		return stationVertrek;
	}

	public String getStationAankomst()
	{
		return stationAankomst;
	}

	public String getPlaatsNaam()
	{
		return plaatsNaam;
	}

	public String getPerron()
	{
		return perron;
	}

	public String getTraject()
	{
		return traject;
	}

	public String getReisTijd()
	{
		return reisTijd;
	}

	public String getReisAfstand()
	{
		return reisAfstand;
	}

	public String getTreinNummer()
	{
		return treinNummer;
	}

	public String getTussenStop()
	{
		return tussenStop;
	}

	public String getTypeTrein()
	{
		return typeTrein;
	}

	public String getEindPunt()
	{
		return eindPunt;
	}

	public ArrayList<LocalTime> getReisTijden()
	{
		return reisTijden;
	}
}
