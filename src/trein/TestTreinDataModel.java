package trein;

import java.util.ArrayList;

public class TestTreinDataModel
{
	public static void main(String [] args)
	{
		runUnitTest();
	}
	
	public static void runUnitTest()
	{
		System.out.println(testCase());
		for (TreinTijd treintijd : JSONTreinReader.getTreinTijd())
		{
			System.out.println(treintijd.getReisTijden());
			System.out.println(treintijd.getReisAfstand());
		}

	}
	
	private static ArrayList<TreinTijd> testCase()
	{
			
		return JSONTreinReader.getTreinTijd();
		
	}
}
