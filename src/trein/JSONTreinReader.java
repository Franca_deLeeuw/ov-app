package trein;
import trein.TreinTijd;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;

public class JSONTreinReader
{
    public static ArrayList<TreinTijd> getTreinTijd()
    {
        JSONParser parser = new JSONParser();
        ArrayList<TreinTijd> treinList = new ArrayList<>();

        try
        {
            JSONArray array = (JSONArray) parser.parse(new FileReader("src/Database/DatabaseTrein.json"));

            for (Object object : array)
            {
                ArrayList<LocalTime> reisTijden = new ArrayList<>();
                reisTijden.clear();

                JSONObject treinObject = (JSONObject) object;

                String stationVertrek = (String) treinObject.get("Vertrekstation");

                String stationAankomst = (String) treinObject.get("Aankomststation");

                String plaatsNaam = (String) treinObject.get("Plaatsnaam");

                String perron = (String) treinObject.get("Perron");

                String traject = (String) treinObject.get("Traject");

                String reisTijd = (String) treinObject.get("Reistijd");

                String reisAfstand = (String) treinObject.get("Reisafstand");

                String treinNummer = (String) treinObject.get("Trein nummer");

                String tussenStop = (String) treinObject.get("Tussen stop");

                String typeTrein = (String) treinObject.get("Type trein");

                String eindPunt = (String) treinObject.get("Eind punt");

                String vertrekTijden = (String) treinObject.get("Vertrektijd");
                String aankomstTijden = (String) treinObject.get("Aankomsttijd");

                String vertrekTijden1 = (String)treinObject.get("Vertrektijd1");
                String aankomstTijden1 = (String) treinObject.get("Aankomsttijd1");

                String vertrekTijden2 = (String)treinObject.get("Vertrektijd2");
                String aankomstTijden2 = (String) treinObject.get("Aankomsttijd2");

                String vertrekTijden3 = (String)treinObject.get("Vertrektijd3");
                String aankomstTijden3 = (String) treinObject.get("Aankomsttijd3");

                String vertrekTijden4 = (String)treinObject.get("Vertrektijd4");
                String aankomstTijden4 = (String) treinObject.get("Aankomsttijd4");

                String vertrekTijden5 = (String)treinObject.get("Vertrektijd5");
                String aankomstTijden5 = (String) treinObject.get("Aankomsttijd5");

                reisTijden.add(LocalTime.parse(vertrekTijden));
                reisTijden.add(LocalTime.parse(aankomstTijden));
                reisTijden.add(LocalTime.parse(vertrekTijden1));
                reisTijden.add(LocalTime.parse(aankomstTijden1));
                reisTijden.add(LocalTime.parse(vertrekTijden2));
                reisTijden.add(LocalTime.parse(aankomstTijden2));
                reisTijden.add(LocalTime.parse(vertrekTijden3));
                reisTijden.add(LocalTime.parse(aankomstTijden3));
                reisTijden.add(LocalTime.parse(vertrekTijden4));
                reisTijden.add(LocalTime.parse(aankomstTijden4));
                reisTijden.add(LocalTime.parse(vertrekTijden5));
                reisTijden.add(LocalTime.parse(aankomstTijden5));

                TreinTijd treinTijd = new TreinTijd(stationVertrek, stationAankomst, plaatsNaam, perron, traject,
                            reisTijd, reisAfstand, treinNummer, tussenStop, typeTrein, eindPunt, reisTijden);
                treinList.add(treinTijd);
            }
        }
        catch (ParseException | IOException e)
        {
            e.printStackTrace();
        }
        return treinList;
    }
}

