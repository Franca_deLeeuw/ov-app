public class Tester
{
    private Boolean overallTestResult = true;

    // Run the tester
    public Boolean runTester()
    {
        System.out.println("\nRun tester..");

        int testCase = 1;
        Boolean testResult = testCase1();
        printTestResult(testCase, testResult);

        printTestResult(2, testCase2());

        return overallTestResult;
    }

    private Boolean testCase1()
    {
        // Step-by-step written
        //
        Profile profile = new Profile("username", "password", "homestation");
        String newUsername = "newUsername";
        profile.setUserName(newUsername);
        // Execute test and save the test result
        Boolean testResult = profile.getUserName().equals(newUsername);
        // Evaluate the result on the overall test result
        testResult = setOverallTestResult(testResult);

        // Return the result of this testcase
        return testResult;
    }

    private Boolean testCase2()
    {
        Profile profile = new Profile("username", "password", "homestation");
        String newPassword = "newPassword";
        profile.setPassword(newPassword);

        Boolean testResult = profile.getPassword().equals(newPassword);

        testResult = setOverallTestResult(testResult);

        return testResult;
    }

    // Set overallTestResult to false when a testCase fails
    private Boolean setOverallTestResult(Boolean testResult)
    {
        if(!testResult)
        {
            overallTestResult = false;
        }

        return testResult;
    }

    // Print the test results
    private void printTestResult(int testCase, Boolean testResult)
    {
        System.out.println("\t#Test result @testCase " + String.valueOf(testCase) + ": " + testResult);
    }

}
