package tram;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;

public class JSONTramReader
{
    public static ArrayList<TramTijd> getTramTijd()
    {
        JSONParser parser = new JSONParser();
        ArrayList<TramTijd> tramList = new ArrayList<>();

        try
        {
            JSONArray array = (JSONArray) parser.parse(new FileReader("src/Database/DatabaseTram.json"));

            for (Object object : array)
            {
                ArrayList<LocalTime> reisTijden = new ArrayList<>();
                reisTijden.clear();

                JSONObject tramObject = (JSONObject) object;

                String stationVertrek = (String) tramObject.get("Vertrekstation");

                String stationAankomst = (String) tramObject.get("Aankomststation");

                String plaatsNaam = (String) tramObject.get("Plaatsnaam");

                String tramHalte = (String) tramObject.get("Tram halte");

                String traject = (String) tramObject.get("Traject");

                String reisTijd = (String) tramObject.get("Reistijd");

                String reisAfstand = (String) tramObject.get("Reisafstand");

                String tramNummer = (String) tramObject.get("Tram nummer");

                String tussenStop = (String) tramObject.get("Tussen stop");

                String eindPunt = (String) tramObject.get("Eind punt");

                String vertrekTijden = (String) tramObject.get("Vertrektijd");
                String aankomstTijden = (String) tramObject.get("Aankomsttijd");

                String vertrekTijden1 = (String)tramObject.get("Vertrektijd1");
                String aankomstTijden1 = (String) tramObject.get("Aankomsttijd1");

                String vertrekTijden2 = (String)tramObject.get("Vertrektijd2");
                String aankomstTijden2 = (String) tramObject.get("Aankomsttijd2");

                String vertrekTijden3 = (String)tramObject.get("Vertrektijd3");
                String aankomstTijden3 = (String) tramObject.get("Aankomsttijd3");

                String vertrekTijden4 = (String)tramObject.get("Vertrektijd4");
                String aankomstTijden4 = (String) tramObject.get("Aankomsttijd4");

                String vertrekTijden5 = (String)tramObject.get("Vertrektijd5");
                String aankomstTijden5 = (String) tramObject.get("Aankomsttijd5");

                reisTijden.add(LocalTime.parse(vertrekTijden));
                reisTijden.add(LocalTime.parse(aankomstTijden));
                reisTijden.add(LocalTime.parse(vertrekTijden1));
                reisTijden.add(LocalTime.parse(aankomstTijden1));
                reisTijden.add(LocalTime.parse(vertrekTijden2));
                reisTijden.add(LocalTime.parse(aankomstTijden2));
                reisTijden.add(LocalTime.parse(vertrekTijden3));
                reisTijden.add(LocalTime.parse(aankomstTijden3));
                reisTijden.add(LocalTime.parse(vertrekTijden4));
                reisTijden.add(LocalTime.parse(aankomstTijden4));
                reisTijden.add(LocalTime.parse(vertrekTijden5));
                reisTijden.add(LocalTime.parse(aankomstTijden5));

                TramTijd tramTijd = new TramTijd(stationVertrek, stationAankomst, plaatsNaam, tramHalte, traject,
                        reisTijd, reisAfstand, tramNummer, tussenStop, eindPunt, reisTijden);
                tramList.add(tramTijd);
            }
        }
        catch (ParseException | IOException e)
        {
            e.printStackTrace();
        }

        return tramList;
    }
}
