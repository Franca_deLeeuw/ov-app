package tram;

import java.time.LocalTime;
import java.util.*;

public class TramTijd
{
	private String stationVertrek;
	private String stationAankomst;
	private String plaatsNaam;
	private String tramHalte;
	private String traject;
	private String tramNummer;
	private String reisTijd;
	private String reisAfstand;
	private	String tussenStop;
	private ArrayList<LocalTime> reisTijden;
	private String eindPunt;

	public TramTijd(String stationVertrek, String stationAankomst, String plaatsNaam, String tramHalte, String traject,
					 String tramNummer, String reisTijd, String reisAfstand, String tussenStop, String eindPunt,ArrayList reisTijden)
	{
		this.stationVertrek = stationVertrek;
		this.stationAankomst = stationAankomst;
		this.plaatsNaam = plaatsNaam;
		this.tramHalte = tramHalte;
		this.traject = traject;
		this.tramNummer = tramNummer;
		this.reisTijd = reisTijd;
		this.reisAfstand = reisAfstand;
		this.tussenStop = tussenStop;
		this.reisTijden = reisTijden;
		this.eindPunt = eindPunt;
	}

	public String getStationVertrek()
	{
		return stationVertrek;
	}

	public String getStationAankomst()
	{
		return stationAankomst;
	}

	public String getPlaatsNaam()
	{
		return plaatsNaam;
	}

	public String getTramHalte()
	{
		return tramHalte;
	}

	public String getTraject()
	{
		return traject;
	}

	public String getTramNummer()
	{
		return tramNummer;
	}

	public String getReisTijd()
	{
		return reisTijd;
	}

	public String getReisAfstand()
	{
		return reisAfstand;
	}

	public String getTussenStop()
	{
		return tussenStop;
	}

	public ArrayList<LocalTime> getReisTijden()
	{
		return reisTijden;
	}

	public String getEindPunt(){return eindPunt;}
}
