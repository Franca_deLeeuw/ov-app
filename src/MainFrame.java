import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;
import javax.swing.plaf.PanelUI;
import javax.swing.table.DefaultTableModel;
import javax.security.auth.Refreshable;
import javax.swing.*;
import bus.BusTijd;
import bus.JSONBusReader;
import metro.JSONMetroReader;
import metro.MetroTijd;
import tram.JSONTramReader;
import tram.TramTijd;
import trein.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class MainFrame extends javax.swing.JFrame
{

    int mouseX;
    int mouseY;
    int languageValue = 0;

    //indexLanguage = 0 betekend Nederlands.
    //indexLanguage = 1 means English.
    int indexLanguage = 0;
    int stayLoggedStatus = 0;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm");
    Profile profile = new Profile("", "", "");
    Profile profile1 = new Profile("Superkoek", "Koekje", "Utrecht Centraal");
    Profile profile2 = new Profile("Hank Anderson", "Fucking Password", "Detroit");
    Profile profile3 = new Profile("admin", "admin", "Amsterdam Centraal");
    Profile profile4 = new Profile("empty", "empty", "empty");
    Locale language = new Locale("EN");
    ResourceBundle languagesBundle = ResourceBundle.getBundle("LanguagesBundle", language);
    interface LocalChangeListener
    {
        void initComponents();
    }

    public MainFrame()
    {
        if(indexLanguage == 0)
        {
            Locale.setDefault(new Locale("NL"));
            languagesBundle = ResourceBundle.getBundle("LanguagesBundle");
        }

        else if(indexLanguage == 1)
        {
            Locale.setDefault(new Locale("EN"));
            languagesBundle = ResourceBundle.getBundle("LanguagesBundle");
        }

        initComponents();
        setLocationRelativeTo(null);
        selectTrein.setVisible(false);
        selectTram.setVisible(false);
        selectMetro.setVisible(false);
        selectBus.setVisible(true);
        mainPanel.add(favorietTrajectPanel);
        favorietTrajectPanel.setVisible(false);
        favorieteTrajectenPanel.setVisible(false);
        languagesPanel.setVisible(false);

        if(stayLoggedStatus == 1)
        {
            profielPanel.setVisible(false);
            loginPanel.setVisible(false);
            messageLabel.setVisible(false);
            messageLabel2.setVisible(false);
            mainPanel.setVisible(true);
        }

        else
        {
            profielPanel.setVisible(false);
            loginPanel.setVisible(true);
            messageLabel.setVisible(false);
            messageLabel2.setVisible(false);
            mainPanel.setVisible(false);
        }
    }

    private void updatePanels()
    {

        Date date = new Date();

        mainPanel = new javax.swing.JPanel();
        selectPanel = new javax.swing.JPanel();
        OVLogo = new javax.swing.JLabel();
        minButton = new javax.swing.JLabel();
        closeButton = new javax.swing.JLabel();
        treinButton = new javax.swing.JLabel();
        busButton = new javax.swing.JLabel();
        metroButton = new javax.swing.JLabel();
        tramButton = new javax.swing.JLabel();
        selectBus = new javax.swing.JPanel();
        selectTrein = new javax.swing.JPanel();
        selectMetro = new javax.swing.JPanel();
        selectTram = new javax.swing.JPanel();
        dragbar = new javax.swing.JLabel();
        usernameLabel = new javax.swing.JLabel();
        userIcon = new javax.swing.JLabel();
        invoerPanel = new javax.swing.JPanel();
        vertrekLabel = new javax.swing.JLabel();
        vertrekpuntLabel = new javax.swing.JLabel();
        datumLabel = new javax.swing.JLabel();
        favorietTrajectButton = new javax.swing.JButton();
        bevestigingButton = new javax.swing.JButton();
        detailButton = new javax.swing.JButton();
        detailPanel = new javax.swing.JPanel();
        detailLabel = new javax.swing.JLabel();
        detailLabelTussenstop = new javax.swing.JLabel();
        detailLabelVoorziening = new javax.swing.JLabel();
        detailLabelVoorziening1 = new javax.swing.JLabel();
        detailLabelVoorziening2 = new javax.swing.JLabel();
        detailLabelVoorziening3 = new javax.swing.JLabel();
        detailknopPanel = new javax.swing.JPanel();
        trajectopslaanButton = new javax.swing.JButton();
        favorietTrajectPanel = new javax.swing.JPanel();
        favorieteTrajectenPanel = new javax.swing.JPanel();
        favorietTrajectLabel = new javax.swing.JLabel();
        tijdstipLabel = new javax.swing.JLabel();
        tijdstipText = new javax.swing.JTextField(simpleTimeFormat.format(date));
        aankomstLabel = new javax.swing.JLabel();
        aankomstText = new javax.swing.JTextField();
        aankomstpuntLabel = new javax.swing.JLabel();
        vertrekText = new javax.swing.JTextField();
        datumText = new javax.swing.JTextField(simpleDateFormat.format(date));
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        messageLabel = new javax.swing.JLabel();
        messageLabel2 = new javax.swing.JLabel();
        loginPanel = new javax.swing.JPanel();
        signInPanel = new javax.swing.JPanel();
        signInLabel2 = new javax.swing.JLabel();
        usernameLabel1 = new javax.swing.JLabel();
        usernameText = new javax.swing.JTextField();
        backgroundLogin = new javax.swing.JLabel();
        passwordLabel = new javax.swing.JLabel();
        passwordText = new javax.swing.JPasswordField();
        stayLoggedCheckBox = new javax.swing.JCheckBox();
        registerLoginButton = new javax.swing.JButton();
        loginButton = new javax.swing.JButton();
        stayLoggedInLabel = new javax.swing.JLabel();
        forgotPasswordLabel = new javax.swing.JLabel();
        loginTitleBarPanel = new javax.swing.JPanel();
        OVLogo1 = new javax.swing.JLabel();
        minButton1 = new javax.swing.JLabel();
        closeButton1 = new javax.swing.JLabel();
        dragbar1 = new javax.swing.JLabel();
        profielPanel = new javax.swing.JPanel();
        uwProfielPanel = new javax.swing.JPanel();
        uwProfielLabel = new javax.swing.JLabel();
        logUitButton = new javax.swing.JButton();
        wijzigenButton = new javax.swing.JButton();
        usernameProfielLabel = new javax.swing.JLabel();
        usernameProfielLabel2 = new javax.swing.JLabel();
        stationProfielLabel = new javax.swing.JLabel();
        stationProfielLabel2 = new javax.swing.JLabel();
        terugButton = new javax.swing.JButton();
        loginTitleBarPanel1 = new javax.swing.JPanel();
        OVLogo2 = new javax.swing.JLabel();
        minButton2 = new javax.swing.JLabel();
        closeButton2 = new javax.swing.JLabel();
        dragbar2 = new javax.swing.JLabel();
        wijzigPanel = new javax.swing.JPanel();
        uwProfielPanel1 = new javax.swing.JPanel();
        uwProfielWijzigLabel = new javax.swing.JLabel();
        usernameProfielWijzigLabel = new javax.swing.JLabel();
        usernameProfielWijzigText = new javax.swing.JTextField();
        stationWijzigLabel = new javax.swing.JLabel();
        stationWijzingText = new javax.swing.JTextField();
        annulerenButton = new javax.swing.JButton();
        opslaanButton = new javax.swing.JButton();
        loginTitleBarPanel2 = new javax.swing.JPanel();
        OVLogo3 = new javax.swing.JLabel();
        minButton3 = new javax.swing.JLabel();
        closeButton3 = new javax.swing.JLabel();
        dragbar3 = new javax.swing.JLabel();
        kaartPanel = new javax.swing.JPanel();
        kaartLabel = new javax.swing.JLabel();
        trajectOpslaanLabel = new javax.swing.JLabel();
        registerPanel = new javax.swing.JPanel();
        registerMainPanel = new javax.swing.JPanel();
        usernameRegisterLabel = new javax.swing.JLabel();
        usernameRegisterText = new javax.swing.JTextField();
        stationRegisterText = new javax.swing.JTextField();
        cancelRegisterButton = new javax.swing.JButton();
        registerRegisterButton = new javax.swing.JButton();
        passwordRegisterText = new javax.swing.JPasswordField();
        passwordRegisterLabel = new javax.swing.JLabel();
        retypePasswordRegisterText = new javax.swing.JPasswordField();
        retypePasswordRegisterLabel = new javax.swing.JLabel();
        registerTitalBarPanel = new javax.swing.JPanel();
        OVLogo4 = new javax.swing.JLabel();
        minButton4 = new javax.swing.JLabel();
        closeButton4 = new javax.swing.JLabel();
        dragbar4 = new javax.swing.JLabel();
        backgroundRegister = new javax.swing.JLabel();
        backgroundProfiel = new javax.swing.JLabel();
        backgroundWijzig = new javax.swing.JLabel();
        registerLabel = new javax.swing.JLabel();
        stationRegisterLabel = new javax.swing.JLabel();
        languageLabel = new javax.swing.JLabel();
        languagesPanel = new javax.swing.JPanel();
        nederlandsLabel = new javax.swing.JLabel();
        englishLabel = new javax.swing.JLabel();
    }

    @SuppressWarnings("unchecked")
    private void initComponents()
    {
        updatePanels();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new java.awt.CardLayout());

        mainPanel.setBackground(new java.awt.Color(28, 28, 74));
        mainPanel.setPreferredSize(new java.awt.Dimension(1200, 800));

        selectPanel.setBackground(new java.awt.Color(0, 0, 0));
        selectPanel.setPreferredSize(new java.awt.Dimension(1200, 30));
        selectPanel.setLayout(null);

        OVLogo.setIcon(new javax.swing.ImageIcon("src/photoLib/OVLogo.png"));
        selectPanel.add(OVLogo);
        OVLogo.setBounds(30, 0, 50, 100);

        minButton.setIcon(new javax.swing.ImageIcon("src/photoLib/Min.png"));
        minButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                minButtonMouseClicked(evt);
            }
        });
        selectPanel.add(minButton);
        minButton.setBounds(1140, 0, 20, 30);

        closeButton.setIcon(new javax.swing.ImageIcon("src/photoLib/Close.png"));
        closeButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        closeButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                closeButtonMouseClicked(evt);
            }
        });
        selectPanel.add(closeButton);
        closeButton.setBounds(1170, 0, 20, 30);

        treinButton.setFont(new java.awt.Font("Futura", 0, 36));
        treinButton.setForeground(new java.awt.Color(167, 167, 167));
        treinButton.setText(languagesBundle.getString("treinButton"));
        treinButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        treinButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                treinButtonMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                treinButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                treinButtonMouseExited(evt);
            }
        });
        selectPanel.add(treinButton);
        treinButton.setBounds(500, 20, 90, 60);

        busButton.setBackground(new java.awt.Color(187, 187, 187));
        busButton.setFont(new java.awt.Font("Futura", 0, 36));
        busButton.setText(languagesBundle.getString("busButton"));
        busButton.setForeground(new java.awt.Color(167, 167, 167));

        busButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        busButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                busButtonMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                busButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                busButtonMouseExited(evt);
            }
        });

        selectPanel.add(busButton);
        busButton.setBounds(160, 20, 70, 60);

        metroButton.setFont(new java.awt.Font("Futura", 0, 36));
        metroButton.setForeground(new java.awt.Color(167, 167, 167));
        metroButton.setText(languagesBundle.getString("metroButton"));
        metroButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        metroButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                metroButtonMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                metroButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                metroButtonMouseExited(evt);
            }
        });
        selectPanel.add(metroButton);
        metroButton.setBounds(250, 20, 110, 60);

        tramButton.setFont(new java.awt.Font("Futura", 0, 36));
        tramButton.setForeground(new java.awt.Color(167, 167, 167));
        tramButton.setText(languagesBundle.getString("tramButton"));
        tramButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tramButton.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tramButtonMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tramButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tramButtonMouseExited(evt);
            }
        });
        selectPanel.add(tramButton);
        tramButton.setBounds(380, 20, 90, 60);

        selectBus.setBackground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout selectBusLayout = new javax.swing.GroupLayout(selectBus);
        selectBus.setLayout(selectBusLayout);
        selectBusLayout.setHorizontalGroup(
            selectBusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE));
        selectBusLayout.setVerticalGroup(
            selectBusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE));

        selectPanel.add(selectBus);
        selectBus.setBounds(170, 90, 40, 10);

        selectTrein.setBackground(new java.awt.Color(255, 204, 51));

        javax.swing.GroupLayout selectTreinLayout = new javax.swing.GroupLayout(selectTrein);
        selectTrein.setLayout(selectTreinLayout);
        selectTreinLayout.setHorizontalGroup(
            selectTreinLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE));
        selectTreinLayout.setVerticalGroup(
            selectTreinLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE));

        selectPanel.add(selectTrein);
        selectTrein.setBounds(520, 90, 40, 10);

        selectMetro.setBackground(new java.awt.Color(0, 255, 153));

        javax.swing.GroupLayout selectMetroLayout = new javax.swing.GroupLayout(selectMetro);
        selectMetro.setLayout(selectMetroLayout);
        selectMetroLayout.setHorizontalGroup(
            selectMetroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE));

        selectMetroLayout.setVerticalGroup(
            selectMetroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE));

        selectPanel.add(selectMetro);
        selectMetro.setBounds(280, 90, 40, 10);

        selectTram.setBackground(new java.awt.Color(51, 153, 255));

        javax.swing.GroupLayout selectTramLayout = new javax.swing.GroupLayout(selectTram);
        selectTram.setLayout(selectTramLayout);
        selectTramLayout.setHorizontalGroup(
            selectTramLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE));

        selectTramLayout.setVerticalGroup(
            selectTramLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE));

        selectPanel.add(selectTram);
        selectTram.setBounds(400, 90, 40, 10);

        dragbar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter()
        {
            public void mouseDragged(java.awt.event.MouseEvent evt)
            {
                dragbarMouseDragged(evt);
            }
        });

        dragbar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                dragbarMousePressed(evt);
            }
        });
        selectPanel.add(dragbar);
        dragbar.setBounds(0, 0, 1130, 30);

        usernameLabel.setFont(new java.awt.Font("Futura", 0, 16));
        usernameLabel.setForeground(new java.awt.Color(167, 167, 167));
        usernameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        usernameLabel.setText(profile.getUserName());
        usernameLabel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        selectPanel.add(usernameLabel);
        usernameLabel.setBounds(940, 40, 190, 50);

        userIcon.setFont(new java.awt.Font("Futura", 0, 56));
        userIcon.setForeground(new java.awt.Color(167, 167, 167));
        userIcon.setIcon(new javax.swing.ImageIcon("src/photoLib/icons8-test-account-48-4.png"));
        userIcon.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        userIcon.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                userIconMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                userIconMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                userIconMousePressed(evt);
            }
        });

        selectPanel.add(userIcon);
        userIcon.setBounds(1140, 40, 50, 50);

        invoerPanel.setBackground(new java.awt.Color(40, 40, 40));
        favorietTrajectPanel.setBackground(new java.awt.Color(40, 40, 40));
        favorieteTrajectenPanel.setBackground(new java.awt.Color(40, 40, 40));

        vertrekLabel.setFont(new java.awt.Font("Futura", 0, 24));
        vertrekLabel.setForeground(new java.awt.Color(255, 255, 255));
        vertrekLabel.setText("<html>" + languagesBundle.getString("vertrekLabel") + "</html>");

        vertrekpuntLabel.setFont(new java.awt.Font("Futura", 0, 16));
        vertrekpuntLabel.setForeground(new java.awt.Color(255, 255, 255));
        vertrekpuntLabel.setText(languagesBundle.getString("vertrekpuntLabel"));

        datumLabel.setFont(new java.awt.Font("Futura", 0, 16));
        datumLabel.setForeground(new java.awt.Color(255, 255, 255));
        datumLabel.setText(languagesBundle.getString("datumLabel"));

        favorietTrajectButton.setBackground(new java.awt.Color(0, 0, 0));
        favorietTrajectButton.setFont(new java.awt.Font("Futura", 1, 18)); // NOI18N
        favorietTrajectButton.setForeground(new java.awt.Color(255, 255, 255));
        favorietTrajectButton.setText("Opgeslagen Trajecten");
        favorietTrajectButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        favorietTrajectButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                favorietTrajectButtonActionPerformed(evt);
            }
        });

        favorietTrajectLabel.setFont(new java.awt.Font("Futura", 0, 16)); // NOI18N
        favorietTrajectLabel.setForeground(new java.awt.Color(255, 255, 255));
        favorietTrajectLabel.setText("Uw Favoriete Trajecten");

        bevestigingButton.setBackground(new java.awt.Color(0, 0, 0));
        bevestigingButton.setFont(new java.awt.Font("Futura", 1, 18));
        bevestigingButton.setForeground(new java.awt.Color(255, 255, 255));
        bevestigingButton.setText(languagesBundle.getString("bevestigingButton"));
        bevestigingButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bevestigingButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bevestigingButtonActionPerformed(evt);
            }
        });

        detailButton.setBackground(new java.awt.Color(0, 0, 0));
        detailButton.setFont(new java.awt.Font("Futura", 1, 18));
        detailButton.setForeground(new java.awt.Color(255, 255, 255));
        detailButton.setText("Details");
        detailButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        detailButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                detailButtonActionPerformed(evt);
            }
        });

        trajectopslaanButton.setBackground(new java.awt.Color(0, 0, 0));
        trajectopslaanButton.setFont(new java.awt.Font("Futura", 1, 18));
        trajectopslaanButton.setForeground(new java.awt.Color(255, 255, 255));
        trajectopslaanButton.setText(languagesBundle.getString("trajectopslaanButton"));
        trajectopslaanButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        trajectopslaanButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                trajectopslaanButtonActionPerformed(evt);
            }
        });

        favorietTrajectLabel.setFont(new java.awt.Font("Futura", 0, 16));
        favorietTrajectLabel.setForeground(new java.awt.Color(255, 255, 255));
        favorietTrajectLabel.setText(languagesBundle.getString("favorietTrajectLabel"));

        tijdstipLabel.setFont(new java.awt.Font("Futura", 0, 16));
        tijdstipLabel.setForeground(new java.awt.Color(255, 255, 255));
        tijdstipLabel.setText(languagesBundle.getString("tijdstipLabel"));

        tijdstipText.setBackground(new java.awt.Color(0, 0, 0));
        tijdstipText.setFont(new java.awt.Font("Futura", 0, 16));
        tijdstipText.setForeground(new java.awt.Color(153, 153, 153));
        tijdstipText.setAlignmentY(2.0F);
        tijdstipText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));
        tijdstipText.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                tijdstipTextActionPerformed(evt);
            }
        });

        aankomstLabel.setFont(new java.awt.Font("Futura", 0, 24));
        aankomstLabel.setForeground(new java.awt.Color(255, 255, 255));
        aankomstLabel.setText(languagesBundle.getString("aankomstLabel"));

        aankomstText.setBackground(new java.awt.Color(0, 0, 0));
        aankomstText.setFont(new java.awt.Font("Futura", 0, 16));
        aankomstText.setForeground(new java.awt.Color(153, 153, 153));
        aankomstText.setAlignmentY(2.0F);
        aankomstText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));
        aankomstText.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                aankomstTextActionPerformed(evt);
            }
        });

        aankomstpuntLabel.setFont(new java.awt.Font("Futura", 0, 16));
        aankomstpuntLabel.setForeground(new java.awt.Color(255, 255, 255));
        aankomstpuntLabel.setText(languagesBundle.getString("aankomstpuntLabel"));

        vertrekText.setBackground(new java.awt.Color(0, 0, 0));
        vertrekText.setFont(new java.awt.Font("Futura", 0, 16));
        vertrekText.setForeground(new java.awt.Color(153, 153, 153));
        vertrekText.setAlignmentY(2.0F);
        vertrekText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));
        vertrekText.setText(profile.getHomeStation());
        vertrekText.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                vertrekTextActionPerformed(evt);
            }
        });

        datumText.setBackground(new java.awt.Color(0, 0, 0));
        datumText.setFont(new java.awt.Font("Futura", 0, 16));
        datumText.setForeground(new java.awt.Color(153, 153, 153));
        datumText.setAlignmentY(2.0F);
        datumText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));
        datumText.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                datumTextActionPerformed(evt);
            }
        });
        detailknopPanel.setVisible(false);

        javax.swing.GroupLayout invoerPanelLayout = new javax.swing.GroupLayout(invoerPanel);
        invoerPanel.setLayout(invoerPanelLayout);
        invoerPanelLayout.setHorizontalGroup(
            invoerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(invoerPanelLayout.createSequentialGroup()
                .addGap(175, 175, 175)
                .addComponent(vertrekLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, invoerPanelLayout.createSequentialGroup()
                .addContainerGap(51, Short.MAX_VALUE)
                .addGroup(invoerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(invoerPanelLayout.createSequentialGroup()
                            .addComponent(tijdstipLabel))
                        .addGroup(invoerPanelLayout.createSequentialGroup()
                            .addComponent(vertrekpuntLabel))
                            .addComponent(datumLabel)
                            .addGroup(invoerPanelLayout.createSequentialGroup()
                        .addGroup(invoerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(datumText, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(vertrekText, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(invoerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(invoerPanelLayout.createSequentialGroup()
                                    .addComponent(bevestigingButton, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                        .addComponent(trajectopslaanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(aankomstText, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, invoerPanelLayout.createSequentialGroup()
                                    .addComponent(aankomstLabel)
                                    .addGap(118, 118, 118)))
                            .addComponent(tijdstipText, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(invoerPanelLayout.createSequentialGroup()
                                .addComponent(favorietTrajectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(detailButton))
                            .addComponent(aankomstpuntLabel)
                            .addComponent(trajectOpslaanLabel))
                        .addGap(69, 69, 69))))
        );
        invoerPanelLayout.setVerticalGroup(
            invoerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(invoerPanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(vertrekLabel)
                .addGap(18, 18, 18)
                .addComponent(vertrekpuntLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(vertrekText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(datumLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(datumText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(tijdstipLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tijdstipText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(invoerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(favorietTrajectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(detailButton, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addComponent(aankomstLabel)
                .addGap(18, 18, 18)
                .addComponent(aankomstpuntLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(aankomstText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(invoerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bevestigingButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(trajectopslaanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                .addComponent(trajectOpslaanLabel)
                .addContainerGap(65, Short.MAX_VALUE))
        );

        kaartPanel.setBounds(1000, 600, 200, 200);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]{
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String []
            {
                    languagesBundle.getString("defaultTableModelVerterkStation"), languagesBundle.getString("defaultTableModelPerron"),
                    languagesBundle.getString("defaultTableModelVertrekTijd"), languagesBundle.getString("defaultTableModelAankomstStation"),
                    languagesBundle.getString("defaultTableModelAankomsttijd"), languagesBundle.getString("defaultTableModelAfstand")
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        favorietTrajectPanel.setLayout(new BoxLayout(favorietTrajectPanel, BoxLayout.Y_AXIS));
        favorietTrajectPanel.add(Box.createRigidArea(new Dimension(0, 40)));
        favorietTrajectPanel.add(favorietTrajectLabel);
        favorietTrajectPanel.add(Box.createRigidArea(new Dimension(0, 20)));

        //styling voor favorietTrajectPanel staat in getTrajectPanel (aangeroepen via "Opgeslagen Trajecten" knop)

        favorietTrajectPanel.setBounds(480, 100, 720, 710);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(selectPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(invoerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(mainPanelLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                        .addComponent(detailknopPanel)
                        .addComponent(detailButton, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(kaartLabel, GroupLayout.PREFERRED_SIZE, 580, GroupLayout.PREFERRED_SIZE))
        ));
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(selectPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(invoerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(detailknopPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                    .addComponent( detailButton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(kaartLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );

        getContentPane().add(mainPanel, "card2");

        loginPanel.setBackground(new java.awt.Color(28, 28, 74));
        loginPanel.setPreferredSize(new java.awt.Dimension(1200, 800));

        signInPanel.setBackground(new java.awt.Color(30, 30, 30, 200));
        signInPanel.setPreferredSize(new java.awt.Dimension(500, 700));

        signInLabel2.setFont(new java.awt.Font("Futura", 0, 24));
        signInLabel2.setForeground(new java.awt.Color(255, 255, 255));
        signInLabel2.setText(languagesBundle.getString("signInLabel2"));

        usernameLabel1.setFont(new java.awt.Font("Futura", 0, 16));
        usernameLabel1.setForeground(new java.awt.Color(255, 255, 255));
        usernameLabel1.setText(languagesBundle.getString("usernameLabel1"));

        usernameText.setBackground(new java.awt.Color(0, 0, 0));
        usernameText.setFont(new java.awt.Font("Futura", 0, 16));
        usernameText.setForeground(new java.awt.Color(153, 153, 153));
        usernameText.setAlignmentY(2.0F);
        usernameText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));
        usernameText.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                usernameTextActionPerformed(evt);
            }
        });

        passwordLabel.setFont(new java.awt.Font("Futura", 0, 16));
        passwordLabel.setForeground(new java.awt.Color(255, 255, 255));
        passwordLabel.setText(languagesBundle.getString("passwordLabel"));

        passwordText.setBackground(new java.awt.Color(0, 0, 0));
        passwordText.setFont(new java.awt.Font("Futura", 0, 16));
        passwordText.setForeground(new java.awt.Color(153, 153, 153));
        passwordText.setAlignmentY(2.0F);
        passwordText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));

        stayLoggedCheckBox.setFont(new java.awt.Font("Futura", 0, 16)); 
        stayLoggedCheckBox.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        stayLoggedCheckBox.setForeground(new java.awt.Color(255, 255, 255));

        loginButton.setBackground(new java.awt.Color(0, 0, 0));
        loginButton.setFont(new java.awt.Font("Futura", 1, 18)); 
        loginButton.setForeground(new java.awt.Color(255, 255, 255));
        loginButton.setText(languagesBundle.getString("loginButton"));
        loginButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        loginButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                loginButtonActionPerformed(evt);
            }
        });

        registerLoginButton.setBackground(new java.awt.Color(198, 0, 116));
        registerLoginButton.setFont(new java.awt.Font("Futura", 1, 18));
        registerLoginButton.setForeground(new java.awt.Color(255, 255, 255));
        registerLoginButton.setText(languagesBundle.getString("registerLoginButton"));
        registerLoginButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        registerLoginButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                registerLoginButtonActionPerformed(evt);
            }
        });

        stayLoggedInLabel.setFont(new java.awt.Font("Futura", 0, 16));
        stayLoggedInLabel.setForeground(new java.awt.Color(255, 255, 255));
        stayLoggedInLabel.setText(languagesBundle.getString("stayLoggedInLabel"));

        forgotPasswordLabel.setFont(new java.awt.Font("Futura", 2, 12));
        forgotPasswordLabel.setForeground(new java.awt.Color(255, 255, 255));
        forgotPasswordLabel.setText(languagesBundle.getString("forgotPasswordLabel"));
        forgotPasswordLabel.setToolTipText(languagesBundle.getString("forgotPasswordLabelToolTip"));

        messageLabel.setFont(new java.awt.Font("Futura", 2, 14));
        messageLabel.setForeground(new java.awt.Color(255, 0, 51));
        messageLabel.setText(languagesBundle.getString("messageLabel"));
        messageLabel2.setFont(new java.awt.Font("Futura", 2, 14));
        messageLabel2.setForeground(new java.awt.Color(255, 0, 51));
        messageLabel2.setText(languagesBundle.getString("messageLabel2"));

        javax.swing.GroupLayout signInPanelLayout = new javax.swing.GroupLayout(signInPanel);
        signInPanel.setLayout(signInPanelLayout);
        signInPanelLayout.setHorizontalGroup(
            signInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(signInPanelLayout.createSequentialGroup()
                .addGap(213, 213, 213)
                .addComponent(signInLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, signInPanelLayout.createSequentialGroup()
                .addContainerGap(69, Short.MAX_VALUE)
                .addGroup(signInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(messageLabel2)
                    .addGroup(signInPanelLayout.createSequentialGroup()
                        .addComponent(stayLoggedCheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stayLoggedInLabel))
                    .addGroup(signInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(signInPanelLayout.createSequentialGroup()
                            .addComponent(loginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(registerLoginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(signInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(usernameLabel1)
                            .addComponent(usernameText)
                            .addComponent(passwordLabel)
                            .addComponent(passwordText, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE))
                        .addComponent(forgotPasswordLabel))
                    .addComponent(messageLabel))
                .addGap(69, 69, 69))
        );
        signInPanelLayout.setVerticalGroup(
            signInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(signInPanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(signInLabel2)
                .addGap(18, 18, 18)
                .addComponent(usernameLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(usernameText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(passwordLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(passwordText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(forgotPasswordLabel)
                .addGap(2, 2, 2)
                .addGroup(signInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(stayLoggedCheckBox)
                    .addComponent(stayLoggedInLabel))
                .addGap(27, 27, 27)
                .addGroup(signInPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(registerLoginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(loginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(27, 27, 27)
                    .addComponent(messageLabel)
                    .addGap(0, 0, 0)
                    .addComponent(messageLabel2)
                .addContainerGap(322, Short.MAX_VALUE))
        );

        loginTitleBarPanel.setBackground(new java.awt.Color(0, 0, 0));
        loginTitleBarPanel.setPreferredSize(new java.awt.Dimension(1200, 30));
        loginTitleBarPanel.setLayout(null);

        OVLogo1.setIcon(new javax.swing.ImageIcon("src/photoLib/OVLogo.png"));
        loginTitleBarPanel.add(OVLogo1);
        OVLogo1.setBounds(30, 0, 50, 100);

        minButton1.setIcon(new javax.swing.ImageIcon("src/photoLib/Min.png"));
        minButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                minButton1MouseClicked(evt);
            }
        });
        loginTitleBarPanel.add(minButton1);
        minButton1.setBounds(1140, 0, 20, 30);

        closeButton1.setIcon(new javax.swing.ImageIcon("src/photoLib/Close.png"));
        closeButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        closeButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeButton1MouseClicked(evt);
            }
        });
        loginTitleBarPanel.add(closeButton1);
        closeButton1.setBounds(1170, 0, 20, 30);

        dragbar1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                dragbar1MouseDragged(evt);
            }
        });
        dragbar1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                dragbar1MousePressed(evt);
            }
        });
        loginTitleBarPanel.add(dragbar1);
        dragbar1.setBounds(0, 0, 1130, 30);

        languageLabel.setFont(new java.awt.Font("Futura", 0, 16)); // NOI18N
        languageLabel.setForeground(new java.awt.Color(167, 167, 167));
        languageLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/settings.png")); // NOI18N
        languageLabel.setText(languagesBundle.getString("languageLabel"));
        languageLabel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        languageLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                languageLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                languageLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                languageLabelMousePressed(evt);
            }
        });
        loginTitleBarPanel.add(languageLabel);
        languageLabel.setBounds(1040, 40, 150, 50);

        loginPanel.add(loginTitleBarPanel);
        loginTitleBarPanel.setBounds(0, 0, 1200, 100);

        languagesPanel.setBackground((new java.awt.Color(30, 30, 30, 200)));

        nederlandsLabel.setFont(new java.awt.Font("Futura", 1, 18)); // NOI18N
        nederlandsLabel.setForeground(new java.awt.Color(204, 204, 204));
        nederlandsLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nederlandsLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/netherlands-flag-square-icon-16.png")); // NOI18N
        nederlandsLabel.setText(languagesBundle.getString("nederlandsLabel"));
        nederlandsLabel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        nederlandsLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                nederlandsLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                nederlandsLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nederlandsLabelMousePressed(evt);
            }
        });

        englishLabel.setFont(new java.awt.Font("Futura", 1, 18)); // NOI18N
        englishLabel.setForeground(new java.awt.Color(204, 204, 204));
        englishLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        englishLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/united-kingdom-flag-square-icon-16.png")); // NOI18N
        englishLabel.setText(languagesBundle.getString("englishLabel"));
        englishLabel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        englishLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                englishLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                englishLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                englishLabelMousePressed(evt);
            }
        });

        javax.swing.GroupLayout languagesPanelLayout = new javax.swing.GroupLayout(languagesPanel);
        languagesPanel.setLayout(languagesPanelLayout);
        languagesPanelLayout.setHorizontalGroup(
            languagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, languagesPanelLayout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addGroup(languagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nederlandsLabel)
                    .addComponent(englishLabel))
                .addGap(24, 24, 24))
        );
        languagesPanelLayout.setVerticalGroup(
            languagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(languagesPanelLayout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addComponent(nederlandsLabel)
                .addGap(18, 18, 18)
                .addComponent(englishLabel)
                .addGap(18, 18, 18))
        );

        loginPanel.add(languagesPanel);
        languagesPanel.setBounds(1020, 100, 180, 104);

        getContentPane().add(loginPanel, "card3");

        javax.swing.GroupLayout loginPanelLayout = new javax.swing.GroupLayout(loginPanel);
        loginPanel.setLayout(loginPanelLayout);
        loginPanelLayout.setHorizontalGroup(
            loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginPanelLayout.createSequentialGroup()
                .addGap(112, 112, 112)
                .addComponent(signInPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(loginTitleBarPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        loginPanelLayout.setVerticalGroup(
            loginPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginPanelLayout.createSequentialGroup()
                .addComponent(loginTitleBarPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(signInPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE))
        );

        backgroundLogin.setIcon(new javax.swing.ImageIcon("src/photoLib/backgroundLogin.png"));
        loginPanel.add(backgroundLogin);
        backgroundLogin.setBounds(0, 100, 1200, 710);

        getContentPane().add(loginPanel, "card3");

        profielPanel.setBackground(new java.awt.Color(28, 28, 74));

        uwProfielPanel.setBackground(new java.awt.Color(30, 30, 30, 200));
        uwProfielPanel.setPreferredSize(new java.awt.Dimension(978, 700));

        uwProfielLabel.setFont(new java.awt.Font("Futura", 0, 24));
        uwProfielLabel.setForeground(new java.awt.Color(255, 255, 255));
        uwProfielLabel.setText(languagesBundle.getString("uwProfielLabel"));

        logUitButton.setBackground(new java.awt.Color(198, 0, 116));
        logUitButton.setFont(new java.awt.Font("Futura", 1, 18));
        logUitButton.setForeground(new java.awt.Color(255, 255, 255));
        logUitButton.setText(languagesBundle.getString("logUitButton"));
        logUitButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        logUitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logUitButtonActionPerformed(evt);
            }
        });

        wijzigenButton.setBackground(new java.awt.Color(0, 0, 0));
        wijzigenButton.setFont(new java.awt.Font("Futura", 1, 18));
        wijzigenButton.setForeground(new java.awt.Color(255, 255, 255));
        wijzigenButton.setText(languagesBundle.getString("wijzigenButton"));
        wijzigenButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        wijzigenButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wijzigenButtonActionPerformed(evt);
            }
        });

        usernameProfielLabel.setFont(new java.awt.Font("Futura", 0, 16));
        usernameProfielLabel.setForeground(new java.awt.Color(255, 255, 255));
        usernameProfielLabel.setText(languagesBundle.getString("usernameProfielLabel"));

        usernameProfielLabel2.setFont(new java.awt.Font("Futura", 1, 16));
        usernameProfielLabel2.setForeground(new java.awt.Color(153, 153, 153));
        usernameProfielLabel2.setAlignmentY(2.0F);

        usernameProfielLabel2.setText(profile.getUserName());

        stationProfielLabel.setFont(new java.awt.Font("Futura", 0, 16));
        stationProfielLabel.setForeground(new java.awt.Color(255, 255, 255));
        stationProfielLabel.setText(languagesBundle.getString("stationProfielLabel"));


        stationProfielLabel2.setFont(new java.awt.Font("Futura", 1, 16));
        stationProfielLabel2.setForeground(new java.awt.Color(153, 153, 153));
        stationProfielLabel2.setAlignmentY(2.0F);

        stationProfielLabel2.setText(profile.getHomeStation());

        terugButton.setBackground(new java.awt.Color(0, 0, 0));
        terugButton.setFont(new java.awt.Font("Futura", 1, 18));
        terugButton.setForeground(new java.awt.Color(255, 255, 255));
        terugButton.setText(languagesBundle.getString("terugButton"));
        terugButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        terugButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                terugButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout uwProfielPanelLayout = new javax.swing.GroupLayout(uwProfielPanel);
        uwProfielPanel.setLayout(uwProfielPanelLayout);
        uwProfielPanelLayout.setHorizontalGroup(
            uwProfielPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(uwProfielPanelLayout.createSequentialGroup()
                .addContainerGap(61, Short.MAX_VALUE)
                .addGroup(uwProfielPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, uwProfielPanelLayout.createSequentialGroup()
                        .addComponent(uwProfielLabel)
                        .addGap(179, 179, 179))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, uwProfielPanelLayout.createSequentialGroup()
                        .addGroup(uwProfielPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(uwProfielPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(stationProfielLabel)
                                .addComponent(stationProfielLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(usernameProfielLabel)
                                .addComponent(usernameProfielLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(uwProfielPanelLayout.createSequentialGroup()
                                .addComponent(wijzigenButton, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(logUitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(terugButton, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(55, 55, 55))))
        );
        uwProfielPanelLayout.setVerticalGroup(
            uwProfielPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(uwProfielPanelLayout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(uwProfielLabel)
                .addGap(43, 43, 43)
                .addComponent(usernameProfielLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(usernameProfielLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(stationProfielLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stationProfielLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(71, 71, 71)
                .addGroup(uwProfielPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(logUitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(wijzigenButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(terugButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(194, Short.MAX_VALUE))
        );

        loginTitleBarPanel1.setBackground(new java.awt.Color(0, 0, 0));
        loginTitleBarPanel1.setPreferredSize(new java.awt.Dimension(1200, 30));
        loginTitleBarPanel1.setLayout(null);

        OVLogo2.setIcon(new javax.swing.ImageIcon("src/photoLib/OVLogo.png")); 
        loginTitleBarPanel1.add(OVLogo2);
        OVLogo2.setBounds(30, 0, 50, 100);

        minButton2.setIcon(new javax.swing.ImageIcon("src/photoLib/Min.png")); 
        minButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                minButton2MouseClicked(evt);
            }
        });
        loginTitleBarPanel1.add(minButton2);
        minButton2.setBounds(1140, 0, 20, 30);

        closeButton2.setIcon(new javax.swing.ImageIcon("src/photoLib/Close.png")); 
        closeButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        closeButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeButton2MouseClicked(evt);
            }
        });
        loginTitleBarPanel1.add(closeButton2);
        closeButton2.setBounds(1170, 0, 20, 30);

        dragbar2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                dragbar2MouseDragged(evt);
            }
        });
        dragbar2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                dragbar2MousePressed(evt);
            }
        });
        loginTitleBarPanel1.add(dragbar2);
        dragbar2.setBounds(0, 0, 1130, 30);

        javax.swing.GroupLayout profielPanelLayout = new javax.swing.GroupLayout(profielPanel);
        profielPanel.setLayout(profielPanelLayout);
        profielPanelLayout.setHorizontalGroup(
            profielPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(loginTitleBarPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(profielPanelLayout.createSequentialGroup()
                .addGap(361, 361, 361)
                .addComponent(uwProfielPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 478, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        profielPanelLayout.setVerticalGroup(
            profielPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(profielPanelLayout.createSequentialGroup()
                .addComponent(loginTitleBarPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(uwProfielPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        backgroundProfiel.setIcon(new javax.swing.ImageIcon("src/photoLib/profileBackground.jpg"));
        profielPanel.add(backgroundProfiel);
        backgroundProfiel.setBounds(0, 100, 1200, 710);

        getContentPane().add(profielPanel, "card3");

        wijzigPanel.setBackground(new java.awt.Color(28, 28, 74));

        backgroundWijzig.setIcon(new javax.swing.ImageIcon("src/photoLib/profileBackground.jpg"));
        wijzigPanel.add(backgroundWijzig);
        backgroundWijzig.setBounds(0, 100, 1200, 710);

        uwProfielPanel1.setBackground(new java.awt.Color(30, 30, 30, 200));
        uwProfielPanel1.setPreferredSize(new java.awt.Dimension(978, 700));

        uwProfielWijzigLabel.setFont(new java.awt.Font("Futura", 0, 24));
        uwProfielWijzigLabel.setForeground(new java.awt.Color(255, 255, 255));
        uwProfielWijzigLabel.setText(languagesBundle.getString("uwProfielWijzigLabel"));

        usernameProfielWijzigLabel.setFont(new java.awt.Font("Futura", 0, 16));
        usernameProfielWijzigLabel.setForeground(new java.awt.Color(255, 255, 255));
        usernameProfielWijzigLabel.setText(languagesBundle.getString("usernameProfielWijzigLabel"));

        usernameProfielWijzigText.setBackground(new java.awt.Color(0, 0, 0));
        usernameProfielWijzigText.setFont(new java.awt.Font("Futura", 0, 16));
        usernameProfielWijzigText.setForeground(new java.awt.Color(153, 153, 153));
        usernameProfielWijzigText.setAlignmentY(2.0F);
        usernameProfielWijzigText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));
        usernameProfielWijzigText.setText(profile.getUserName());
        usernameProfielWijzigText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usernameProfielWijzigTextActionPerformed(evt);
            }
        });

        stationWijzigLabel.setFont(new java.awt.Font("Futura", 0, 16));
        stationWijzigLabel.setForeground(new java.awt.Color(255, 255, 255));
        stationWijzigLabel.setText(languagesBundle.getString("stationWijzigLabel"));

        stationWijzingText.setBackground(new java.awt.Color(0, 0, 0));
        stationWijzingText.setFont(new java.awt.Font("Futura", 0, 16));
        stationWijzingText.setForeground(new java.awt.Color(153, 153, 153));
        stationWijzingText.setAlignmentY(2.0F);
        stationWijzingText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));
        stationWijzingText.setText(profile.getHomeStation());
        stationWijzingText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stationWijzingTextActionPerformed(evt);
            }
        });

        annulerenButton.setBackground(new java.awt.Color(198, 0, 116));
        annulerenButton.setFont(new java.awt.Font("Futura", 1, 18));
        annulerenButton.setForeground(new java.awt.Color(255, 255, 255));
        annulerenButton.setText(languagesBundle.getString("annulerenButton"));
        annulerenButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        annulerenButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerenButtonActionPerformed(evt);
            }
        });

        opslaanButton.setBackground(new java.awt.Color(0, 0, 0));
        opslaanButton.setFont(new java.awt.Font("Futura", 1, 18));
        opslaanButton.setForeground(new java.awt.Color(255, 255, 255));
        opslaanButton.setText(languagesBundle.getString("opslaanButton"));
        opslaanButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        opslaanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opslaanButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout uwProfielPanel1Layout = new javax.swing.GroupLayout(uwProfielPanel1);
        uwProfielPanel1.setLayout(uwProfielPanel1Layout);
        uwProfielPanel1Layout.setHorizontalGroup(
            uwProfielPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(uwProfielPanel1Layout.createSequentialGroup()
                .addContainerGap(61, Short.MAX_VALUE)
                .addGroup(uwProfielPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, uwProfielPanel1Layout.createSequentialGroup()
                        .addGroup(uwProfielPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(uwProfielPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(stationWijzigLabel)
                                .addComponent(stationWijzingText, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(usernameProfielWijzigLabel)
                                .addComponent(usernameProfielWijzigText, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(uwProfielPanel1Layout.createSequentialGroup()
                                .addComponent(opslaanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(annulerenButton, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(55, 55, 55))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, uwProfielPanel1Layout.createSequentialGroup()
                        .addComponent(uwProfielWijzigLabel)
                        .addGap(179, 179, 179))))
        );
        uwProfielPanel1Layout.setVerticalGroup(
            uwProfielPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(uwProfielPanel1Layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(uwProfielWijzigLabel)
                .addGap(43, 43, 43)
                .addComponent(usernameProfielWijzigLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(usernameProfielWijzigText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(stationWijzigLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stationWijzingText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(71, 71, 71)
                .addGroup(uwProfielPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(annulerenButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(opslaanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(266, Short.MAX_VALUE))
        );

        loginTitleBarPanel2.setBackground(new java.awt.Color(0, 0, 0));
        loginTitleBarPanel2.setPreferredSize(new java.awt.Dimension(1200, 30));
        loginTitleBarPanel2.setLayout(null);

        OVLogo3.setIcon(new javax.swing.ImageIcon("src/photoLib/OVLogo.png"));
        loginTitleBarPanel2.add(OVLogo3);
        OVLogo3.setBounds(30, 0, 50, 100);

        minButton3.setIcon(new javax.swing.ImageIcon("src/photoLib/Min.png"));
        minButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                minButton3MouseClicked(evt);
            }
        });
        loginTitleBarPanel2.add(minButton3);
        minButton3.setBounds(1140, 0, 20, 30);

        closeButton3.setIcon(new javax.swing.ImageIcon("src/photoLib/Close.png")); 
        closeButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        closeButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeButton3MouseClicked(evt);
            }
        });
        loginTitleBarPanel2.add(closeButton3);
        closeButton3.setBounds(1170, 0, 20, 30);

        dragbar3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                dragbar3MouseDragged(evt);
            }
        });
        dragbar3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                dragbar3MousePressed(evt);
            }
        });
        loginTitleBarPanel2.add(dragbar3);
        dragbar3.setBounds(0, 0, 1130, 30);

        javax.swing.GroupLayout wijzigPanelLayout = new javax.swing.GroupLayout(wijzigPanel);
        wijzigPanel.setLayout(wijzigPanelLayout);
        wijzigPanelLayout.setHorizontalGroup(
            wijzigPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(loginTitleBarPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(wijzigPanelLayout.createSequentialGroup()
                .addGap(361, 361, 361)
                .addComponent(uwProfielPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 478, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        wijzigPanelLayout.setVerticalGroup(
            wijzigPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(wijzigPanelLayout.createSequentialGroup()
                .addComponent(loginTitleBarPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(uwProfielPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        backgroundWijzig.setIcon(new javax.swing.ImageIcon("src/photoLib/profileBackground.jpg")); 
        wijzigPanel.add(backgroundWijzig);
        backgroundWijzig.setBounds(0, 100, 1200, 710);

        getContentPane().add(wijzigPanel, "card3");

        registerPanel.setBackground(new java.awt.Color(28, 28, 74));
        registerPanel.setLayout(null);

        registerMainPanel.setBackground(new java.awt.Color(30, 30, 30, 200));
        registerMainPanel.setPreferredSize(new java.awt.Dimension(978, 700));

        registerLabel.setFont(new java.awt.Font("Futura", 0, 24));
        registerLabel.setForeground(new java.awt.Color(255, 255, 255));
        registerLabel.setText(languagesBundle.getString("registerLabel"));

        usernameRegisterLabel.setFont(new java.awt.Font("Futura", 0, 16));
        usernameRegisterLabel.setForeground(new java.awt.Color(255, 255, 255));
        usernameRegisterLabel.setText(languagesBundle.getString("usernameRegisterLabel"));

        usernameRegisterText.setBackground(new java.awt.Color(0, 0, 0));
        usernameRegisterText.setFont(new java.awt.Font("Futura", 0, 16));
        usernameRegisterText.setForeground(new java.awt.Color(153, 153, 153));
        usernameRegisterText.setAlignmentY(2.0F);
        usernameRegisterText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));
        usernameRegisterText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usernameRegisterTextActionPerformed(evt);
            }
        });

        stationRegisterLabel.setFont(new java.awt.Font("Futura", 0, 16));
        stationRegisterLabel.setForeground(new java.awt.Color(255, 255, 255));
        stationRegisterLabel.setText(languagesBundle.getString("stationRegisterLabel"));

        stationRegisterText.setBackground(new java.awt.Color(0, 0, 0));
        stationRegisterText.setFont(new java.awt.Font("Futura", 0, 16));
        stationRegisterText.setForeground(new java.awt.Color(153, 153, 153));
        stationRegisterText.setAlignmentY(2.0F);
        stationRegisterText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));
        stationRegisterText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stationRegisterTextActionPerformed(evt);
            }
        });

        cancelRegisterButton.setBackground(new java.awt.Color(198, 0, 116));
        cancelRegisterButton.setFont(new java.awt.Font("Futura", 1, 18));
        cancelRegisterButton.setForeground(new java.awt.Color(255, 255, 255));
        cancelRegisterButton.setText(languagesBundle.getString("cancelRegisterButton"));
        cancelRegisterButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancelRegisterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelRegisterButtonActionPerformed(evt);
            }
        });

        registerRegisterButton.setBackground(new java.awt.Color(0, 0, 0));
        registerRegisterButton.setFont(new java.awt.Font("Futura", 1, 18));
        registerRegisterButton.setForeground(new java.awt.Color(255, 255, 255));
        registerRegisterButton.setText(languagesBundle.getString("registerRegisterButton"));
        registerRegisterButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        registerRegisterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerRegisterButtonActionPerformed(evt);
            }
        });

        passwordRegisterText.setBackground(new java.awt.Color(0, 0, 0));
        passwordRegisterText.setFont(new java.awt.Font("Futura", 0, 16));
        passwordRegisterText.setForeground(new java.awt.Color(153, 153, 153));
        passwordRegisterText.setAlignmentY(2.0F);
        passwordRegisterText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));

        passwordRegisterLabel.setFont(new java.awt.Font("Futura", 0, 16));
        passwordRegisterLabel.setForeground(new java.awt.Color(255, 255, 255));
        passwordRegisterLabel.setText(languagesBundle.getString("passwordRegisterLabel"));

        retypePasswordRegisterText.setBackground(new java.awt.Color(0, 0, 0));
        retypePasswordRegisterText.setFont(new java.awt.Font("Futura", 0, 16));
        retypePasswordRegisterText.setForeground(new java.awt.Color(153, 153, 153));
        retypePasswordRegisterText.setAlignmentY(2.0F);
        retypePasswordRegisterText.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));

        retypePasswordRegisterLabel.setFont(new java.awt.Font("Futura", 0, 16));
        retypePasswordRegisterLabel.setForeground(new java.awt.Color(255, 255, 255));
        retypePasswordRegisterLabel.setText(languagesBundle.getString("retypePasswordRegisterLabel"));

        javax.swing.GroupLayout registerMainPanelLayout = new javax.swing.GroupLayout(registerMainPanel);
        registerMainPanel.setLayout(registerMainPanelLayout);
        registerMainPanelLayout.setHorizontalGroup(
                registerMainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(registerMainPanelLayout.createSequentialGroup()
                                .addGap(195, 195, 195)
                                .addComponent(registerLabel)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registerMainPanelLayout.createSequentialGroup()
                                .addContainerGap(61, Short.MAX_VALUE)
                                .addGroup(registerMainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(registerMainPanelLayout.createSequentialGroup()
                                                .addComponent(registerRegisterButton, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(cancelRegisterButton, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(registerMainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(passwordRegisterLabel)
                                                .addComponent(passwordRegisterText, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(stationRegisterLabel)
                                                .addComponent(stationRegisterText, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(usernameRegisterLabel)
                                                .addComponent(usernameRegisterText, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(retypePasswordRegisterLabel)
                                                .addComponent(retypePasswordRegisterText, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(55, 55, 55))
        );
        registerMainPanelLayout.setVerticalGroup(
                registerMainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(registerMainPanelLayout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(registerLabel)
                                .addGap(33, 33, 33)
                                .addComponent(usernameRegisterLabel)
                                .addGap(5, 5, 5)
                                .addComponent(usernameRegisterText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(stationRegisterLabel)
                                .addGap(5, 5, 5)
                                .addComponent(stationRegisterText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(passwordRegisterLabel)
                                .addGap(5, 5, 5)
                                .addComponent(passwordRegisterText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(retypePasswordRegisterLabel)
                                .addGap(5, 5, 5)
                                .addComponent(retypePasswordRegisterText, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(50, 50, 50)
                                .addGroup(registerMainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(registerRegisterButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(cancelRegisterButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(97, Short.MAX_VALUE))
        );

        registerPanel.add(registerMainPanel);
        registerMainPanel.setBounds(361, 100, 478, 710);

        registerTitalBarPanel.setBackground(new java.awt.Color(0, 0, 0));
        registerTitalBarPanel.setPreferredSize(new java.awt.Dimension(1200, 30));
        registerTitalBarPanel.setLayout(null);

        OVLogo4.setIcon(new javax.swing.ImageIcon("src/photoLib/OVLogo.png"));
        registerTitalBarPanel.add(OVLogo4);
        OVLogo4.setBounds(30, 0, 50, 100);

        minButton4.setIcon(new javax.swing.ImageIcon("src/photoLib/Min.png"));
        minButton4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                minButton4MouseClicked(evt);
            }
        });
        registerTitalBarPanel.add(minButton4);
        minButton4.setBounds(1140, 0, 20, 30);

        closeButton4.setIcon(new javax.swing.ImageIcon("src/photoLib/Close.png"));
        closeButton4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        closeButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeButton4MouseClicked(evt);
            }
        });
        registerTitalBarPanel.add(closeButton4);
        closeButton4.setBounds(1170, 0, 20, 30);

        dragbar4.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                dragbar4MouseDragged(evt);
            }
        });
        dragbar4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                dragbar4MousePressed(evt);
            }
        });
        registerTitalBarPanel.add(dragbar4);
        dragbar4.setBounds(0, 0, 1130, 30);

        registerPanel.add(registerTitalBarPanel);
        registerTitalBarPanel.setBounds(0, 0, 1200, 100);
        backgroundRegister.setIcon(new javax.swing.ImageIcon("src/photoLib/backgroundLogin.png"));
        registerPanel.add(backgroundRegister);
        backgroundRegister.setBounds(0, 100, 1200, 710);

        getContentPane().add(registerPanel, "card6");

        pack();
    }
    int klik = 1;
    private void detailButtonActionPerformed(ActionEvent evt) {
        if (evt.getSource() == detailButton) {
            if (selectTrein.isVisible()) {
                for (TreinTijd treinTijd : JSONTreinReader.getTreinTijd()) {
                    if (vertrekText.getText().toLowerCase().equals(treinTijd.getStationVertrek().toLowerCase())) {
                        if(klik == 1) {

                            jScrollPane1.setVisible(false);
                            detailknopPanel.setVisible(true);

                            detailknopPanel.setLayout(null);
                            detailLabel.setText("De " + treinTijd.getTypeTrein() + " van " + vertrekText.getText() + " naar " + treinTijd.getEindPunt());
                            detailLabel.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabel.setBounds(120, 5, 500, 20);

                            detailLabelTussenstop.setText("Tussenstop: " + treinTijd.getTussenStop());
                            detailLabelTussenstop.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabelTussenstop.setBounds(150, 40, 400, 20);

                            detailLabelVoorziening.setText("Voorzieningen: ");
                            detailLabelVoorziening.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabelVoorziening.setBounds(240, 70, 400, 25);

                            detailLabelVoorziening1.setText("- Wifi");
                            detailLabelVoorziening1.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening1.setBounds(240, 100, 400, 25);

                            detailLabelVoorziening2.setText("- Diverse oplaadpunten");
                            detailLabelVoorziening2.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening2.setBounds(240, 130, 400, 25);

                            detailLabelVoorziening3.setText("- Extra bagage ruimte");
                            detailLabelVoorziening3.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening3.setBounds(240, 160, 400, 25);

                            detailknopPanel.add(detailLabel);
                            detailknopPanel.add(detailLabelTussenstop);
                            detailknopPanel.add(detailLabelVoorziening);
                            detailknopPanel.add(detailLabelVoorziening1);
                            detailknopPanel.add(detailLabelVoorziening2);
                            detailknopPanel.add(detailLabelVoorziening3);
                            klik = 2;
                        } else{
                            detailknopPanel.setVisible(false);
                            jScrollPane1.setVisible(true);
                            klik = 1;
                        }
                    }
                }
            } else if (selectMetro.isVisible()) {
                for (MetroTijd metroTijd : JSONMetroReader.getMetroTijd()) {
                    if (vertrekText.getText().toLowerCase().equals(metroTijd.getStationVertrek().toLowerCase())) {
                        if (klik == 1) {

                            jScrollPane1.setVisible(false);
                            detailknopPanel.setVisible(true);

                            detailknopPanel.setLayout(null);
                            detailLabel.setText("De metro van " + vertrekText.getText() + " naar " + metroTijd.getEindPunt());
                            detailLabel.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabel.setBounds(120, 5, 500, 20);

                            detailLabelTussenstop.setText("Tussenstop: " + metroTijd.getTussenStop());
                            detailLabelTussenstop.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabelTussenstop.setBounds(150, 40, 400, 20);

                            detailLabelVoorziening.setText("Voorzieningen: ");
                            detailLabelVoorziening.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabelVoorziening.setBounds(240, 70, 400, 25);

                            detailLabelVoorziening1.setText("- Wifi");
                            detailLabelVoorziening1.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening1.setBounds(240, 100, 400, 25);

                            detailLabelVoorziening2.setText("- Diverse oplaadpunten");
                            detailLabelVoorziening2.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening2.setBounds(240, 130, 400, 25);

                            detailLabelVoorziening3.setText("- Extra bagage ruimte");
                            detailLabelVoorziening3.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening3.setBounds(240, 160, 400, 25);

                            detailknopPanel.add(detailLabel);
                            detailknopPanel.add(detailLabelTussenstop);
                            detailknopPanel.add(detailLabelVoorziening);
                            detailknopPanel.add(detailLabelVoorziening1);
                            detailknopPanel.add(detailLabelVoorziening2);
                            detailknopPanel.add(detailLabelVoorziening3);
                            klik = 2;
                        } else {
                            detailknopPanel.setVisible(false);
                            jScrollPane1.setVisible(true);
                            klik = 1;
                        }
                    }

                }
            } else if (selectTram.isVisible()) {
                for (TramTijd tramTijd : JSONTramReader.getTramTijd()) {
                    if (vertrekText.getText().toLowerCase().equals(tramTijd.getStationVertrek().toLowerCase())) {
                        if (klik == 1) {

                            jScrollPane1.setVisible(false);
                            detailknopPanel.setVisible(true);

                            detailknopPanel.setLayout(null);
                            detailLabel.setText("De tram van " + vertrekText.getText() + " naar " + tramTijd.getEindPunt());
                            detailLabel.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabel.setBounds(120, 5, 500, 20);

                            detailLabelTussenstop.setText("Tussenstop: " + tramTijd.getTussenStop());
                            detailLabelTussenstop.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabelTussenstop.setBounds(150, 40, 400, 20);

                            detailLabelVoorziening.setText("Voorzieningen: ");
                            detailLabelVoorziening.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabelVoorziening.setBounds(240, 70, 400, 25);

                            detailLabelVoorziening1.setText("- Wifi");
                            detailLabelVoorziening1.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening1.setBounds(240, 100, 400, 25);

                            detailLabelVoorziening2.setText("- Diverse oplaadpunten");
                            detailLabelVoorziening2.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening2.setBounds(240, 130, 400, 25);

                            detailLabelVoorziening3.setText("- Prullenbak");
                            detailLabelVoorziening3.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening3.setBounds(240, 160, 400, 25);

                            detailknopPanel.add(detailLabel);
                            detailknopPanel.add(detailLabelTussenstop);
                            detailknopPanel.add(detailLabelVoorziening);
                            detailknopPanel.add(detailLabelVoorziening1);
                            detailknopPanel.add(detailLabelVoorziening2);
                            detailknopPanel.add(detailLabelVoorziening3);
                            klik = 2;
                        } else {
                            detailknopPanel.setVisible(false);
                            jScrollPane1.setVisible(true);
                            klik = 1;
                        }
                    }
                }
            } else if (selectBus.isVisible()) {
                for (BusTijd busTijd : JSONBusReader.getBusTijd()) {
                    if (vertrekText.getText().toLowerCase().equals(busTijd.getStationVertrek().toLowerCase())) {
                        if (klik == 1) {

                            jScrollPane1.setVisible(false);
                            detailknopPanel.setVisible(true);

                            detailknopPanel.setLayout(null);
                            detailLabel.setText("De " + busTijd.getTypeBus() +" van " + vertrekText.getText() + " naar " + busTijd.getEindPunt());
                            detailLabel.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabel.setBounds(120, 5, 500, 20);

                            detailLabelTussenstop.setText("Tussenstop: " + busTijd.getTussenStop());
                            detailLabelTussenstop.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabelTussenstop.setBounds(150, 40, 400, 20);

                            detailLabelVoorziening.setText("Voorzieningen: ");
                            detailLabelVoorziening.setFont(new Font("futura", Font.BOLD, 16));
                            detailLabelVoorziening.setBounds(240, 70, 400, 25);

                            detailLabelVoorziening1.setText("- Wifi");
                            detailLabelVoorziening1.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening1.setBounds(240, 100, 400, 25);

                            detailLabelVoorziening2.setText("- Diverse oplaadpunten");
                            detailLabelVoorziening2.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening2.setBounds(240, 130, 400, 25);

                            detailLabelVoorziening3.setText("- Prullenbak");
                            detailLabelVoorziening3.setFont(new Font("futura", Font.PLAIN, 16));
                            detailLabelVoorziening3.setBounds(240, 160, 400, 25);

                            detailknopPanel.add(detailLabel);
                            detailknopPanel.add(detailLabelTussenstop);
                            detailknopPanel.add(detailLabelVoorziening);
                            detailknopPanel.add(detailLabelVoorziening1);
                            detailknopPanel.add(detailLabelVoorziening2);
                            detailknopPanel.add(detailLabelVoorziening3);
                            klik = 2;
                        } else {
                            detailknopPanel.setVisible(false);
                            jScrollPane1.setVisible(true);
                            klik = 1;
                        }
                    }
                }
            }
        }
    }
    private JPanel getTrajectenPanel()
    {
        if (selectBus.isVisible())
        {
            // int t om de iteraties in de for loop te tellen van het aantal trajecten in de lijst
            int t = 0;
            for (int tt = 0; tt < profile.getFavorietTrajectBus().size(); tt++)
            {
                for (BusTijd busTijd : JSONBusReader.getBusTijd())
                {
                    if (busTijd.getTraject().equals(String.valueOf(profile.getFavorietTrajectBus().get(tt))))
                    {
                        for (Integer trajectTijd = 0; trajectTijd < profile.getFavorietTrajectBusTijd().size(); trajectTijd++)
                        {
                            // check of de datum en traject bij elkaar horen
                            if (t==trajectTijd)
                            {
                                int q = t;
                                JPanel trajectPanel = new JPanel();
                                trajectPanel.setLayout(null);
                                trajectPanel.setBackground(new Color(40, 40, 40));
                                favorieteTrajectenPanel.add(trajectPanel);
                                JLabel favorietTrajectPlaatsLabel = new JLabel("<html>Traject van: <b style='color:999999'>"+busTijd.getStationVertrek()+"</b> naar: <b style='color:999999'>"+busTijd.getStationAankomst()+"</b></html>");
                                JLabel favorietTrajectTijdLabel = new JLabel("<html>Vanaf <b style='color:999999'>"+profile.getFavorietTrajectBusTijd().get(trajectTijd)+"</b> uur</html>");
                                favorietTrajectPlaatsLabel.setForeground(new Color(255 ,255 ,255));
                                favorietTrajectPlaatsLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                favorietTrajectPlaatsLabel.setBounds(20, 0, 400, 20);
                                favorietTrajectTijdLabel.setForeground(new Color(255 ,255 ,255));
                                favorietTrajectTijdLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                favorietTrajectTijdLabel.setBounds(20, 34, 200, 20);

                                // bewerken
                                JLabel bewerkVertrekLabel = new JLabel("Traject van: ");
                                bewerkVertrekLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkVertrekLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkVertrekLabel.setBounds(20, 1, 82, 20);
                                JTextField bewerkVertrekTextField = new JTextField(busTijd.getStationVertrek());
                                bewerkVertrekTextField.setBounds(102, 0, 140, 25);
                                JLabel bewerkAankomstLabel = new JLabel("naar:");
                                bewerkAankomstLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkAankomstLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkAankomstLabel.setBounds(245, 1, 42, 20);
                                JTextField bewerkAankomstTextField = new JTextField(busTijd.getStationAankomst());
                                bewerkAankomstTextField.setBounds(287, 0, 140, 25);
                                JLabel bewerkTijdLabel = new JLabel("Vanaf");
                                bewerkTijdLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkTijdLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkTijdLabel.setBounds(20, 35, 50, 20);
                                JTextField bewerkTijdTextField = new JTextField(String.valueOf(profile.getFavorietTrajectBusTijd().get(trajectTijd)));
                                bewerkTijdTextField.setBounds(63, 32, 50, 25);
                                JLabel bewerkTijdLabel2 = new JLabel("uur");
                                bewerkTijdLabel2.setForeground(new Color(255 ,255 ,255));
                                bewerkTijdLabel2.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkTijdLabel2.setBounds(116, 35, 45, 20);

                                // functionele knoppen
                                JButton kiesTrajectButton = new JButton("Selecteer Traject");
                                kiesTrajectButton.setBackground(new Color(0, 0, 0));
                                kiesTrajectButton.setForeground(new Color(255, 255, 255));
                                kiesTrajectButton.setBounds(20, 65, 120, 20);
                                JButton verwijderTrajectButton = new JButton("Verwijderen");
                                verwijderTrajectButton.setBackground(new Color(0, 0, 0));
                                verwijderTrajectButton.setForeground(new Color(255, 255, 255));
                                verwijderTrajectButton.setBounds(180, 65, 100, 20);
                                JButton bewerkTrajectButton = new JButton("Bewerken");
                                bewerkTrajectButton.setBackground(new Color(0, 0, 0));
                                bewerkTrajectButton.setForeground(new Color(255, 255, 255));
                                bewerkTrajectButton.setBounds(280, 65, 90, 20);

                                trajectPanel.add(favorietTrajectPlaatsLabel);
                                trajectPanel.add(favorietTrajectTijdLabel);
                                trajectPanel.add(kiesTrajectButton);
                                trajectPanel.add(verwijderTrajectButton);
                                trajectPanel.add(bewerkTrajectButton);

                                kiesTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        String verwerking1Text = favorietTrajectPlaatsLabel.getText().replace("<html>Traject van: <b style='color:999999'>", "");
                                        String verwerking2Text = verwerking1Text.replace("</b> naar: <b style='color:999999'>", ";");
                                        String verwerking3Text = verwerking2Text.replace("</b></html>", "");
                                        String[] plaatsen = verwerking3Text.split(";");
                                        vertrekText.setText(plaatsen[0]);
                                        aankomstText.setText(plaatsen[1]);
                                        tijdstipText.setText(String.valueOf(profile.getFavorietTrajectBusTijd().get(q)));
                                        jScrollPane1.setVisible(true);
                                        kaartLabel.setVisible(true);
                                        favorietTrajectPanel.setVisible(false);
                                        favorieteTrajectenPanel.setVisible(false);
                                        favorieteTrajectenPanel.removeAll();

                                        bevestigingButtonActionPerformed(evt);
                                    }
                                });

                                verwijderTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        switch(verwijderTrajectButton.getText())
                                        {
                                            case"Verwijderen":

                                                String verwerking1Text = favorietTrajectPlaatsLabel.getText().replace("<html>Traject van: <b style='color:999999'>", "");
                                                String verwerking2Text = verwerking1Text.replace("</b> naar: <b style='color:999999'>", ";");
                                                String verwerking3Text = verwerking2Text.replace("</b></html>", "");
                                                String[] plaatsen = verwerking3Text.split(";");

                                                for (BusTijd busTijd : JSONBusReader.getBusTijd())
                                                {
                                                    if (busTijd.getStationVertrek().toLowerCase().equals(plaatsen[0].toLowerCase())&&busTijd.getStationAankomst().toLowerCase().equals(plaatsen[1].toLowerCase()))
                                                    {
                                                        for (int i = 0; i < profile.getFavorietTrajectBus().size(); i++)
                                                        {
                                                            if (profile.getFavorietTrajectBus().get(i).equals(Integer.valueOf(busTijd.getTraject())))
                                                            {
                                                                profile.removeFavorietTrajectBus(i);

                                                                // leegmaken van de panel
                                                                trajectPanel.removeAll();
                                                                trajectPanel.repaint();
                                                            }
                                                        }
                                                    }
                                                }
                                                break;

                                            case"Ontdoen":

                                                trajectPanel.add(favorietTrajectTijdLabel);
                                                trajectPanel.add(favorietTrajectPlaatsLabel);
                                                trajectPanel.remove(bewerkAankomstLabel);
                                                trajectPanel.remove(bewerkAankomstTextField);
                                                trajectPanel.remove(bewerkVertrekLabel);
                                                trajectPanel.remove(bewerkVertrekTextField);
                                                trajectPanel.remove(bewerkTijdLabel);
                                                trajectPanel.remove(bewerkTijdTextField);
                                                trajectPanel.remove(bewerkTijdLabel2);
                                                trajectPanel.repaint();

                                                verwijderTrajectButton.setText("Verwijderen");
                                                bewerkTrajectButton.setText("Bewerken");

                                                break;
                                        }


                                    }
                                });

                                bewerkTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        switch(bewerkTrajectButton.getText())
                                        {
                                            case"Bewerken":

                                                trajectPanel.remove(favorietTrajectTijdLabel);
                                                trajectPanel.remove(favorietTrajectPlaatsLabel);
                                                trajectPanel.add(bewerkAankomstLabel);
                                                trajectPanel.add(bewerkAankomstTextField);
                                                trajectPanel.add(bewerkVertrekLabel);
                                                trajectPanel.add(bewerkVertrekTextField);
                                                trajectPanel.add(bewerkTijdLabel);
                                                trajectPanel.add(bewerkTijdTextField);
                                                trajectPanel.add(bewerkTijdLabel2);
                                                trajectPanel.repaint();

                                                bewerkTrajectButton.setText("Opslaan");
                                                verwijderTrajectButton.setText("Ontdoen");

                                                break;

                                            case"Opslaan":

                                                // gewijzigde informatie opslaan
                                                for (int b = 0; b < JSONBusReader.getBusTijd().size(); b++)
                                                {
                                                    if (JSONBusReader.getBusTijd().get(b).getStationVertrek().toLowerCase().equals(bewerkVertrekTextField.getText().toLowerCase())&&JSONBusReader.getBusTijd().get(b).getStationAankomst().toLowerCase().equals(bewerkAankomstTextField.getText().toLowerCase()))
                                                    {
                                                        for (int i = 0; i < profile.getFavorietTrajectBus().size(); i++)
                                                        {
                                                            if (profile.getFavorietTrajectBus().get(i).equals(Integer.valueOf(busTijd.getTraject())))
                                                            {
                                                                profile.removeFavorietTrajectBus(i);

                                                                // nieuwe check voor invoeren nieuw favoriet traject
                                                                for (BusTijd busTijd2 : JSONBusReader.getBusTijd())
                                                                {
                                                                    if (busTijd2.getStationVertrek().toLowerCase().equals(bewerkVertrekTextField.getText().toLowerCase())
                                                                            && busTijd2.getStationAankomst().toLowerCase().equals(bewerkAankomstTextField.getText().toLowerCase()))
                                                                    {

                                                                        profile.setFavorietTrajectBus(Integer.valueOf(busTijd2.getTraject()), LocalTime.parse(bewerkTijdTextField.getText()));
                                                                        System.out.println("nieuw traject toegevoegd");

                                                                        // labels updaten
                                                                        favorietTrajectTijdLabel.setText("<html>Vanaf <b style='color:999999'>"+bewerkTijdTextField.getText()+" uur</html>");
                                                                        favorietTrajectPlaatsLabel.setText("<html>Traject van: <b style='color:999999'>"+busTijd2.getStationVertrek()+"</b> naar: <b style='color:999999'>"+busTijd2.getStationAankomst()+"</b></html>");

                                                                        // refreshen van de panel
                                                                        trajectPanel.remove(bewerkAankomstLabel);
                                                                        trajectPanel.remove(bewerkAankomstTextField);
                                                                        trajectPanel.remove(bewerkVertrekLabel);
                                                                        trajectPanel.remove(bewerkVertrekTextField);
                                                                        trajectPanel.remove(bewerkTijdLabel);
                                                                        trajectPanel.remove(bewerkTijdTextField);
                                                                        trajectPanel.remove(bewerkTijdLabel2);
                                                                        trajectPanel.add(favorietTrajectTijdLabel);
                                                                        trajectPanel.add(favorietTrajectPlaatsLabel);
                                                                        trajectPanel.repaint();

                                                                        bewerkTrajectButton.setText("Bewerken");
                                                                        verwijderTrajectButton.setText("Verwijderen");

                                                                        break;
                                                                    }

                                                                }
                                                                break;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        System.out.println("Geen overeenkomst.\nAantal gecheckte bus modellen: "+b);
                                                    }
                                                    if (b==JSONBusReader.getBusTijd().size()-1)
                                                    {
                                                        System.out.println("definitief geen match gevonden!");
                                                        JOptionPane.showMessageDialog(mainPanel, "Dit traject staat niet in ons systeem. (Controleer mogelijk op spelfouten)",
                                                        "Verkeerde invoer", JOptionPane.WARNING_MESSAGE);

                                                        bewerkVertrekTextField.setText(busTijd.getStationVertrek());
                                                        bewerkAankomstTextField.setText(busTijd.getStationAankomst());

                                                        break;
                                                    }
                                                }
                                            break;
                                        }

                                    }
                                });

                                break;
                            }

                        }
                        break;
                    }

                }

                // 1 iteratie optellen
                t++;
            }

            if (t>5)
            {
                favorieteTrajectenPanel.setLayout(new GridLayout(5, 2, 40, 40));
            } else if (t<=4)
            {
                favorieteTrajectenPanel.setLayout(new GridLayout(5, 1, 40, 40));
            }


        }

        else if (selectTram.isVisible())
        {
            // int t om de iteraties in de for loop te tellen van het aantal trajecten in de lijst
            int t = 0;
            for (int tt = 0; tt < profile.getFavorietTrajectTram().size(); tt++)
            {
                for (TramTijd tramTijd : JSONTramReader.getTramTijd())
                {
                    if (tramTijd.getTraject().equals(String.valueOf(profile.getFavorietTrajectTram().get(tt))))
                    {
                        for (Integer trajectTijd = 0; trajectTijd < profile.getFavorietTrajectTramTijd().size(); trajectTijd++)
                        {
                            // check of de datum en traject bij elkaar horen
                            if (t==trajectTijd)
                            {
                                int q = t;
                                JPanel trajectPanel = new JPanel();
                                trajectPanel.setLayout(null);
                                trajectPanel.setBackground(new Color(40, 40, 40));
                                favorieteTrajectenPanel.add(trajectPanel);
                                JLabel favorietTrajectPlaatsLabel = new JLabel("<html>Traject van: <b style='color:999999'>"+tramTijd.getStationVertrek()+"</b> naar: <b style='color:999999'>"+tramTijd.getStationAankomst()+"</b></html>");
                                JLabel favorietTrajectTijdLabel = new JLabel("<html>Vanaf <b style='color:999999'>"+profile.getFavorietTrajectTramTijd().get(trajectTijd)+"</b> uur</html>");
                                favorietTrajectPlaatsLabel.setForeground(new Color(255 ,255 ,255));
                                favorietTrajectPlaatsLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                favorietTrajectPlaatsLabel.setBounds(20, 0, 400, 20);
                                favorietTrajectTijdLabel.setForeground(new Color(255 ,255 ,255));
                                favorietTrajectTijdLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                favorietTrajectTijdLabel.setBounds(20, 34, 200, 20);

                                // bewerken
                                JLabel bewerkVertrekLabel = new JLabel("Traject van: ");
                                bewerkVertrekLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkVertrekLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkVertrekLabel.setBounds(20, 1, 82, 20);
                                JTextField bewerkVertrekTextField = new JTextField(tramTijd.getStationVertrek());
                                bewerkVertrekTextField.setBounds(102, 0, 140, 25);
                                JLabel bewerkAankomstLabel = new JLabel("naar:");
                                bewerkAankomstLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkAankomstLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkAankomstLabel.setBounds(245, 1, 42, 20);
                                JTextField bewerkAankomstTextField = new JTextField(tramTijd.getStationAankomst());
                                bewerkAankomstTextField.setBounds(287, 0, 140, 25);
                                JLabel bewerkTijdLabel = new JLabel("Vanaf");
                                bewerkTijdLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkTijdLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkTijdLabel.setBounds(20, 35, 50, 20);
                                JTextField bewerkTijdTextField = new JTextField(String.valueOf(profile.getFavorietTrajectTramTijd().get(trajectTijd)));
                                bewerkTijdTextField.setBounds(63, 32, 50, 25);
                                JLabel bewerkTijdLabel2 = new JLabel("uur");
                                bewerkTijdLabel2.setForeground(new Color(255 ,255 ,255));
                                bewerkTijdLabel2.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkTijdLabel2.setBounds(116, 35, 45, 20);

                                // functionele knoppen
                                JButton kiesTrajectButton = new JButton("Selecteer Traject");
                                kiesTrajectButton.setBackground(new Color(0, 0, 0));
                                kiesTrajectButton.setForeground(new Color(255, 255, 255));
                                kiesTrajectButton.setBounds(20, 65, 120, 20);
                                JButton verwijderTrajectButton = new JButton("Verwijderen");
                                verwijderTrajectButton.setBackground(new Color(0, 0, 0));
                                verwijderTrajectButton.setForeground(new Color(255, 255, 255));
                                verwijderTrajectButton.setBounds(180, 65, 100, 20);
                                JButton bewerkTrajectButton = new JButton("Bewerken");
                                bewerkTrajectButton.setBackground(new Color(0, 0, 0));
                                bewerkTrajectButton.setForeground(new Color(255, 255, 255));
                                bewerkTrajectButton.setBounds(280, 65, 90, 20);

                                trajectPanel.add(favorietTrajectPlaatsLabel);
                                trajectPanel.add(favorietTrajectTijdLabel);
                                trajectPanel.add(kiesTrajectButton);
                                trajectPanel.add(verwijderTrajectButton);
                                trajectPanel.add(bewerkTrajectButton);

                                kiesTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        String verwerking1Text = favorietTrajectPlaatsLabel.getText().replace("<html>Traject van: <b style='color:999999'>", "");
                                        String verwerking2Text = verwerking1Text.replace("</b> naar: <b style='color:999999'>", ";");
                                        String verwerking3Text = verwerking2Text.replace("</b></html>", "");
                                        String[] plaatsen = verwerking3Text.split(";");
                                        vertrekText.setText(plaatsen[0]);
                                        aankomstText.setText(plaatsen[1]);
                                        tijdstipText.setText(String.valueOf(profile.getFavorietTrajectTramTijd().get(q)));
                                        jScrollPane1.setVisible(true);
                                        kaartLabel.setVisible(true);
                                        favorietTrajectPanel.setVisible(false);
                                        favorieteTrajectenPanel.setVisible(false);
                                        favorieteTrajectenPanel.removeAll();

                                        bevestigingButtonActionPerformed(evt);
                                    }
                                });

                                verwijderTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        switch(verwijderTrajectButton.getText())
                                        {
                                            case"Verwijderen":

                                                String verwerking1Text = favorietTrajectPlaatsLabel.getText().replace("<html>Traject van: <b style='color:999999'>", "");
                                                String verwerking2Text = verwerking1Text.replace("</b> naar: <b style='color:999999'>", ";");
                                                String verwerking3Text = verwerking2Text.replace("</b></html>", "");
                                                String[] plaatsen = verwerking3Text.split(";");

                                                for (TramTijd tramTijd : JSONTramReader.getTramTijd())
                                                {
                                                    if (tramTijd.getStationVertrek().toLowerCase().equals(plaatsen[0].toLowerCase())&&tramTijd.getStationAankomst().toLowerCase().equals(plaatsen[1].toLowerCase()))
                                                    {
                                                        for (int i = 0; i < profile.getFavorietTrajectTram().size(); i++)
                                                        {
                                                            if (profile.getFavorietTrajectTram().get(i).equals(Integer.valueOf(tramTijd.getTraject())))
                                                            {
                                                                profile.removeFavorietTrajectTram(i);

                                                                // leegmaken van de panel
                                                                trajectPanel.removeAll();
                                                                trajectPanel.repaint();
                                                            }
                                                        }
                                                    }
                                                }
                                                break;

                                            case"Ontdoen":

                                                trajectPanel.add(favorietTrajectTijdLabel);
                                                trajectPanel.add(favorietTrajectPlaatsLabel);
                                                trajectPanel.remove(bewerkAankomstLabel);
                                                trajectPanel.remove(bewerkAankomstTextField);
                                                trajectPanel.remove(bewerkVertrekLabel);
                                                trajectPanel.remove(bewerkVertrekTextField);
                                                trajectPanel.remove(bewerkTijdLabel);
                                                trajectPanel.remove(bewerkTijdTextField);
                                                trajectPanel.remove(bewerkTijdLabel2);
                                                trajectPanel.repaint();

                                                verwijderTrajectButton.setText("Verwijderen");
                                                bewerkTrajectButton.setText("Bewerken");

                                                break;
                                        }


                                    }
                                });

                                bewerkTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        switch(bewerkTrajectButton.getText())
                                        {
                                            case"Bewerken":

                                                trajectPanel.remove(favorietTrajectTijdLabel);
                                                trajectPanel.remove(favorietTrajectPlaatsLabel);
                                                trajectPanel.add(bewerkAankomstLabel);
                                                trajectPanel.add(bewerkAankomstTextField);
                                                trajectPanel.add(bewerkVertrekLabel);
                                                trajectPanel.add(bewerkVertrekTextField);
                                                trajectPanel.add(bewerkTijdLabel);
                                                trajectPanel.add(bewerkTijdTextField);
                                                trajectPanel.add(bewerkTijdLabel2);
                                                trajectPanel.repaint();

                                                bewerkTrajectButton.setText("Opslaan");
                                                verwijderTrajectButton.setText("Ontdoen");

                                                break;

                                            case"Opslaan":

                                                // gewijzigde informatie opslaan
                                                for (int b = 0; b < JSONTramReader.getTramTijd().size(); b++)
                                                {
                                                    if (JSONTramReader.getTramTijd().get(b).getStationVertrek().toLowerCase().equals(bewerkVertrekTextField.getText().toLowerCase())&&JSONTramReader.getTramTijd().get(b).getStationAankomst().toLowerCase().equals(bewerkAankomstTextField.getText().toLowerCase()))
                                                    {
                                                        for (int i = 0; i < profile.getFavorietTrajectTram().size(); i++)
                                                        {
                                                            if (profile.getFavorietTrajectTram().get(i).equals(Integer.valueOf(tramTijd.getTraject())))
                                                            {
                                                                profile.removeFavorietTrajectTram(i);

                                                                // nieuwe check voor invoeren nieuw favoriet traject
                                                                for (TramTijd tramTijd2 : JSONTramReader.getTramTijd())
                                                                {
                                                                    if (tramTijd2.getStationVertrek().toLowerCase().equals(bewerkVertrekTextField.getText().toLowerCase())
                                                                            && tramTijd2.getStationAankomst().toLowerCase().equals(bewerkAankomstTextField.getText().toLowerCase()))
                                                                    {

                                                                        profile.setFavorietTrajectTram(Integer.valueOf(tramTijd2.getTraject()), LocalTime.parse(bewerkTijdTextField.getText()));
                                                                        System.out.println("nieuw traject toegevoegd");

                                                                        // labels updaten
                                                                        favorietTrajectTijdLabel.setText("<html>Vanaf <b style='color:999999'>"+bewerkTijdTextField.getText()+" uur</html>");
                                                                        favorietTrajectPlaatsLabel.setText("<html>Traject van: <b style='color:999999'>"+tramTijd2.getStationVertrek()+"</b> naar: <b style='color:999999'>"+tramTijd2.getStationAankomst()+"</b></html>");

                                                                        // refreshen van de panel
                                                                        trajectPanel.remove(bewerkAankomstLabel);
                                                                        trajectPanel.remove(bewerkAankomstTextField);
                                                                        trajectPanel.remove(bewerkVertrekLabel);
                                                                        trajectPanel.remove(bewerkVertrekTextField);
                                                                        trajectPanel.remove(bewerkTijdLabel);
                                                                        trajectPanel.remove(bewerkTijdTextField);
                                                                        trajectPanel.remove(bewerkTijdLabel2);
                                                                        trajectPanel.add(favorietTrajectTijdLabel);
                                                                        trajectPanel.add(favorietTrajectPlaatsLabel);
                                                                        trajectPanel.repaint();

                                                                        bewerkTrajectButton.setText("Bewerken");
                                                                        verwijderTrajectButton.setText("Verwijderen");

                                                                        break;
                                                                    }

                                                                }
                                                                break;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        System.out.println("Geen overeenkomst.\nAantal gecheckte tram modellen: "+b);
                                                    }
                                                    if (b==JSONTramReader.getTramTijd().size()-1)
                                                    {
                                                        System.out.println("definitief geen match gevonden!");
                                                        JOptionPane.showMessageDialog(mainPanel, "Dit traject staat niet in ons systeem. (Controleer mogelijk op spelfouten)",
                                                        "Verkeerde invoer", JOptionPane.WARNING_MESSAGE);

                                                        bewerkVertrekTextField.setText(tramTijd.getStationVertrek());
                                                        bewerkAankomstTextField.setText(tramTijd.getStationAankomst());

                                                        break;
                                                    }
                                                }
                                            break;
                                        }

                                    }
                                });

                                break;
                            }

                        }
                        break;
                    }

                }

                // 1 iteratie optellen
                t++;
            }

            if (t>5)
            {
                favorieteTrajectenPanel.setLayout(new GridLayout(5, 2, 40, 40));
            } else if (t<=4)
            {
                favorieteTrajectenPanel.setLayout(new GridLayout(5, 1, 40, 40));
            }


        }

        else if (selectTrein.isVisible())
        {
            // int t om de iteraties in de for loop te tellen van het aantal trajecten in de lijst
            int t = 0;
            for (int tt = 0; tt < profile.getFavorietTrajectTrein().size(); tt++)
            {
                for (TreinTijd treinTijd : JSONTreinReader.getTreinTijd())
                {
                    if (treinTijd.getTraject().equals(String.valueOf(profile.getFavorietTrajectTrein().get(tt))))
                    {
                        for (Integer trajectTijd = 0; trajectTijd < profile.getFavorietTrajectTreinTijd().size(); trajectTijd++)
                        {
                            // check of de datum en traject bij elkaar horen
                            if (t==trajectTijd)
                            {
                                int q = t;
                                JPanel trajectPanel = new JPanel();
                                trajectPanel.setLayout(null);
                                trajectPanel.setBackground(new Color(40, 40, 40));
                                favorieteTrajectenPanel.add(trajectPanel);
                                JLabel favorietTrajectPlaatsLabel = new JLabel("<html>Traject van: <b style='color:999999'>"+treinTijd.getStationVertrek()+"</b> naar: <b style='color:999999'>"+treinTijd.getStationAankomst()+"</b></html>");
                                JLabel favorietTrajectTijdLabel = new JLabel("<html>Vanaf <b style='color:999999'>"+profile.getFavorietTrajectTreinTijd().get(trajectTijd)+"</b> uur</html>");
                                favorietTrajectPlaatsLabel.setForeground(new Color(255 ,255 ,255));
                                favorietTrajectPlaatsLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                favorietTrajectPlaatsLabel.setBounds(20, 0, 400, 20);
                                favorietTrajectTijdLabel.setForeground(new Color(255 ,255 ,255));
                                favorietTrajectTijdLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                favorietTrajectTijdLabel.setBounds(20, 34, 200, 20);

                                // bewerken
                                JLabel bewerkVertrekLabel = new JLabel("Traject van: ");
                                bewerkVertrekLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkVertrekLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkVertrekLabel.setBounds(20, 1, 82, 20);
                                JTextField bewerkVertrekTextField = new JTextField(treinTijd.getStationVertrek());
                                bewerkVertrekTextField.setBounds(102, 0, 140, 25);
                                JLabel bewerkAankomstLabel = new JLabel("naar:");
                                bewerkAankomstLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkAankomstLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkAankomstLabel.setBounds(245, 1, 42, 20);
                                JTextField bewerkAankomstTextField = new JTextField(treinTijd.getStationAankomst());
                                bewerkAankomstTextField.setBounds(287, 0, 140, 25);
                                JLabel bewerkTijdLabel = new JLabel("Vanaf");
                                bewerkTijdLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkTijdLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkTijdLabel.setBounds(20, 35, 50, 20);
                                JTextField bewerkTijdTextField = new JTextField(String.valueOf(profile.getFavorietTrajectTreinTijd().get(trajectTijd)));
                                bewerkTijdTextField.setBounds(63, 32, 50, 25);
                                JLabel bewerkTijdLabel2 = new JLabel("uur");
                                bewerkTijdLabel2.setForeground(new Color(255 ,255 ,255));
                                bewerkTijdLabel2.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkTijdLabel2.setBounds(116, 35, 45, 20);

                                // functionele knoppen
                                JButton kiesTrajectButton = new JButton("Selecteer Traject");
                                kiesTrajectButton.setBackground(new Color(0, 0, 0));
                                kiesTrajectButton.setForeground(new Color(255, 255, 255));
                                kiesTrajectButton.setBounds(20, 65, 120, 20);
                                JButton verwijderTrajectButton = new JButton("Verwijderen");
                                verwijderTrajectButton.setBackground(new Color(0, 0, 0));
                                verwijderTrajectButton.setForeground(new Color(255, 255, 255));
                                verwijderTrajectButton.setBounds(180, 65, 100, 20);
                                JButton bewerkTrajectButton = new JButton("Bewerken");
                                bewerkTrajectButton.setBackground(new Color(0, 0, 0));
                                bewerkTrajectButton.setForeground(new Color(255, 255, 255));
                                bewerkTrajectButton.setBounds(280, 65, 90, 20);

                                trajectPanel.add(favorietTrajectPlaatsLabel);
                                trajectPanel.add(favorietTrajectTijdLabel);
                                trajectPanel.add(kiesTrajectButton);
                                trajectPanel.add(verwijderTrajectButton);
                                trajectPanel.add(bewerkTrajectButton);

                                kiesTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        String verwerking1Text = favorietTrajectPlaatsLabel.getText().replace("<html>Traject van: <b style='color:999999'>", "");
                                        String verwerking2Text = verwerking1Text.replace("</b> naar: <b style='color:999999'>", ";");
                                        String verwerking3Text = verwerking2Text.replace("</b></html>", "");
                                        String[] plaatsen = verwerking3Text.split(";");
                                        vertrekText.setText(plaatsen[0]);
                                        aankomstText.setText(plaatsen[1]);
                                        tijdstipText.setText(String.valueOf(profile.getFavorietTrajectTreinTijd().get(q)));
                                        jScrollPane1.setVisible(true);
                                        kaartLabel.setVisible(true);
                                        favorietTrajectPanel.setVisible(false);
                                        favorieteTrajectenPanel.setVisible(false);
                                        favorieteTrajectenPanel.removeAll();

                                        bevestigingButtonActionPerformed(evt);
                                    }
                                });

                                verwijderTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        switch(verwijderTrajectButton.getText())
                                        {
                                            case"Verwijderen":

                                                String verwerking1Text = favorietTrajectPlaatsLabel.getText().replace("<html>Traject van: <b style='color:999999'>", "");
                                                String verwerking2Text = verwerking1Text.replace("</b> naar: <b style='color:999999'>", ";");
                                                String verwerking3Text = verwerking2Text.replace("</b></html>", "");
                                                String[] plaatsen = verwerking3Text.split(";");

                                                for (TreinTijd treinTijd : JSONTreinReader.getTreinTijd())
                                                {
                                                    if (treinTijd.getStationVertrek().toLowerCase().equals(plaatsen[0].toLowerCase())&&treinTijd.getStationAankomst().toLowerCase().equals(plaatsen[1].toLowerCase()))
                                                    {
                                                        for (int i = 0; i < profile.getFavorietTrajectTrein().size(); i++)
                                                        {
                                                            if (profile.getFavorietTrajectTrein().get(i).equals(Integer.valueOf(treinTijd.getTraject())))
                                                            {
                                                                profile.removeFavorietTrajectTrein(i);

                                                                // leegmaken van de panel
                                                                trajectPanel.removeAll();
                                                                trajectPanel.repaint();
                                                            }
                                                        }
                                                    }
                                                }
                                                break;

                                            case"Ontdoen":

                                                trajectPanel.add(favorietTrajectTijdLabel);
                                                trajectPanel.add(favorietTrajectPlaatsLabel);
                                                trajectPanel.remove(bewerkAankomstLabel);
                                                trajectPanel.remove(bewerkAankomstTextField);
                                                trajectPanel.remove(bewerkVertrekLabel);
                                                trajectPanel.remove(bewerkVertrekTextField);
                                                trajectPanel.remove(bewerkTijdLabel);
                                                trajectPanel.remove(bewerkTijdTextField);
                                                trajectPanel.remove(bewerkTijdLabel2);
                                                trajectPanel.repaint();

                                                verwijderTrajectButton.setText("Verwijderen");
                                                bewerkTrajectButton.setText("Bewerken");

                                                break;
                                        }


                                    }
                                });

                                bewerkTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        switch(bewerkTrajectButton.getText())
                                        {
                                            case"Bewerken":

                                                trajectPanel.remove(favorietTrajectTijdLabel);
                                                trajectPanel.remove(favorietTrajectPlaatsLabel);
                                                trajectPanel.add(bewerkAankomstLabel);
                                                trajectPanel.add(bewerkAankomstTextField);
                                                trajectPanel.add(bewerkVertrekLabel);
                                                trajectPanel.add(bewerkVertrekTextField);
                                                trajectPanel.add(bewerkTijdLabel);
                                                trajectPanel.add(bewerkTijdTextField);
                                                trajectPanel.add(bewerkTijdLabel2);
                                                trajectPanel.repaint();

                                                bewerkTrajectButton.setText("Opslaan");
                                                verwijderTrajectButton.setText("Ontdoen");

                                                break;

                                            case"Opslaan":

                                                // gewijzigde informatie opslaan
                                                for (int b = 0; b < JSONTreinReader.getTreinTijd().size(); b++)
                                                {
                                                    System.out.println("checken voor overeenkomst tussen\n"+JSONTreinReader.getTreinTijd().get(b).getStationVertrek()+" en "+bewerkVertrekTextField.getText()+"\n"+JSONTreinReader.getTreinTijd().get(b).getStationAankomst()+" en "+bewerkAankomstTextField.getText());
                                                    if (JSONTreinReader.getTreinTijd().get(b).getStationVertrek().toLowerCase().equals(bewerkVertrekTextField.getText().toLowerCase())&&JSONTreinReader.getTreinTijd().get(b).getStationAankomst().toLowerCase().equals(bewerkAankomstTextField.getText().toLowerCase()))
                                                    {
                                                        System.out.println("overeenkomst!");
                                                        for (int i = 0; i < profile.getFavorietTrajectTrein().size(); i++)
                                                        {
                                                            if (profile.getFavorietTrajectTrein().get(i).equals(Integer.valueOf(treinTijd.getTraject())))
                                                            {
                                                                profile.removeFavorietTrajectTrein(i);
                                                                System.out.println("oude traject verwijderd");

                                                                // nieuwe check voor invoeren nieuw favoriet traject
                                                                for (TreinTijd treinTijd2 : JSONTreinReader.getTreinTijd())
                                                                {
                                                                    if (treinTijd2.getStationVertrek().toLowerCase().equals(bewerkVertrekTextField.getText().toLowerCase())
                                                                            && treinTijd2.getStationAankomst().toLowerCase().equals(bewerkAankomstTextField.getText().toLowerCase()))
                                                                    {

                                                                        profile.setFavorietTrajectTrein(Integer.valueOf(treinTijd2.getTraject()), LocalTime.parse(bewerkTijdTextField.getText()));
                                                                        System.out.println("nieuw traject toegevoegd");

                                                                        // labels updaten
                                                                        favorietTrajectTijdLabel.setText("<html>Vanaf <b style='color:999999'>"+bewerkTijdTextField.getText()+" uur</html>");
                                                                        favorietTrajectPlaatsLabel.setText("<html>Traject van: <b style='color:999999'>"+bewerkVertrekTextField.getText()+"</b> naar: <b style='color:999999'>"+bewerkAankomstTextField.getText()+"</b></html>");

                                                                        // refreshen van de panel
                                                                        trajectPanel.remove(bewerkAankomstLabel);
                                                                        trajectPanel.remove(bewerkAankomstTextField);
                                                                        trajectPanel.remove(bewerkVertrekLabel);
                                                                        trajectPanel.remove(bewerkVertrekTextField);
                                                                        trajectPanel.remove(bewerkTijdLabel);
                                                                        trajectPanel.remove(bewerkTijdTextField);
                                                                        trajectPanel.remove(bewerkTijdLabel2);
                                                                        trajectPanel.add(favorietTrajectTijdLabel);
                                                                        trajectPanel.add(favorietTrajectPlaatsLabel);
                                                                        trajectPanel.repaint();

                                                                        bewerkTrajectButton.setText("Bewerken");
                                                                        verwijderTrajectButton.setText("Verwijderen");

                                                                        break;
                                                                    }

                                                                }
                                                                break;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        System.out.println("Geen overeenkomst.\nAantal gecheckte trein modellen: "+b);
                                                    }
                                                    if (b==JSONTreinReader.getTreinTijd().size()-1)
                                                    {
                                                        System.out.println("definitief geen match gevonden!");
                                                        JOptionPane.showMessageDialog(mainPanel, "Dit traject staat niet in ons systeem. (Controleer mogelijk op spelfouten)",
                                                        "Verkeerde invoer", JOptionPane.WARNING_MESSAGE);

                                                        bewerkVertrekTextField.setText(treinTijd.getStationVertrek());
                                                        bewerkAankomstTextField.setText(treinTijd.getStationAankomst());

                                                        break;
                                                    }
                                                }
                                            break;
                                        }

                                    }
                                });

                                break;
                            }

                        }
                        break;
                    }

                }

                // 1 iteratie optellen
                t++;
            }

            if (t>5)
            {
                favorieteTrajectenPanel.setLayout(new GridLayout(5, 2, 40, 40));
            } else if (t<=4)
            {
                favorieteTrajectenPanel.setLayout(new GridLayout(5, 1, 40, 40));
            }


        }

        else if (selectMetro.isVisible())
        {
            // int t om de iteraties in de for loop te tellen van het aantal trajecten in de lijst
            int t = 0;
            for (int tt = 0; tt < profile.getFavorietTrajectMetro().size(); tt++)
            {
                for (MetroTijd metroTijd : JSONMetroReader.getMetroTijd())
                {
                    if (metroTijd.getTraject().equals(String.valueOf(profile.getFavorietTrajectMetro().get(tt))))
                    {
                        for (Integer trajectTijd = 0; trajectTijd < profile.getFavorietTrajectMetroTijd().size(); trajectTijd++)
                        {
                            // check of de datum en traject bij elkaar horen
                            if (t==trajectTijd)
                            {
                                int q = t;
                                JPanel trajectPanel = new JPanel();
                                trajectPanel.setLayout(null);
                                trajectPanel.setBackground(new Color(40, 40, 40));
                                favorieteTrajectenPanel.add(trajectPanel);
                                JLabel favorietTrajectPlaatsLabel = new JLabel("<html>Traject van: <b style='color:999999'>"+metroTijd.getStationVertrek()+"</b> naar: <b style='color:999999'>"+metroTijd.getStationAankomst()+"</b></html>");
                                JLabel favorietTrajectTijdLabel = new JLabel("<html>Vanaf <b style='color:999999'>"+profile.getFavorietTrajectMetroTijd().get(trajectTijd)+"</b> uur</html>");
                                favorietTrajectPlaatsLabel.setForeground(new Color(255 ,255 ,255));
                                favorietTrajectPlaatsLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                favorietTrajectPlaatsLabel.setBounds(20, 0, 400, 20);
                                favorietTrajectTijdLabel.setForeground(new Color(255 ,255 ,255));
                                favorietTrajectTijdLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                favorietTrajectTijdLabel.setBounds(20, 34, 200, 20);

                                // bewerken
                                JLabel bewerkVertrekLabel = new JLabel("Traject van: ");
                                bewerkVertrekLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkVertrekLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkVertrekLabel.setBounds(20, 1, 82, 20);
                                JTextField bewerkVertrekTextField = new JTextField(metroTijd.getStationVertrek());
                                bewerkVertrekTextField.setBounds(102, 0, 140, 25);
                                JLabel bewerkAankomstLabel = new JLabel("naar:");
                                bewerkAankomstLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkAankomstLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkAankomstLabel.setBounds(245, 1, 42, 20);
                                JTextField bewerkAankomstTextField = new JTextField(metroTijd.getStationAankomst());
                                bewerkAankomstTextField.setBounds(287, 0, 140, 25);
                                JLabel bewerkTijdLabel = new JLabel("Vanaf");
                                bewerkTijdLabel.setForeground(new Color(255 ,255 ,255));
                                bewerkTijdLabel.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkTijdLabel.setBounds(20, 35, 50, 20);
                                JTextField bewerkTijdTextField = new JTextField(String.valueOf(profile.getFavorietTrajectMetroTijd().get(trajectTijd)));
                                bewerkTijdTextField.setBounds(63, 32, 50, 25);
                                JLabel bewerkTijdLabel2 = new JLabel("uur");
                                bewerkTijdLabel2.setForeground(new Color(255 ,255 ,255));
                                bewerkTijdLabel2.setFont(new Font("Futura", 1, 13)); // NOI18N
                                bewerkTijdLabel2.setBounds(116, 35, 45, 20);

                                // functionele knoppen
                                JButton kiesTrajectButton = new JButton("Selecteer Traject");
                                kiesTrajectButton.setBackground(new Color(0, 0, 0));
                                kiesTrajectButton.setForeground(new Color(255, 255, 255));
                                kiesTrajectButton.setBounds(20, 65, 120, 20);
                                JButton verwijderTrajectButton = new JButton("Verwijderen");
                                verwijderTrajectButton.setBackground(new Color(0, 0, 0));
                                verwijderTrajectButton.setForeground(new Color(255, 255, 255));
                                verwijderTrajectButton.setBounds(180, 65, 100, 20);
                                JButton bewerkTrajectButton = new JButton("Bewerken");
                                bewerkTrajectButton.setBackground(new Color(0, 0, 0));
                                bewerkTrajectButton.setForeground(new Color(255, 255, 255));
                                bewerkTrajectButton.setBounds(280, 65, 90, 20);

                                trajectPanel.add(favorietTrajectPlaatsLabel);
                                trajectPanel.add(favorietTrajectTijdLabel);
                                trajectPanel.add(kiesTrajectButton);
                                trajectPanel.add(verwijderTrajectButton);
                                trajectPanel.add(bewerkTrajectButton);

                                kiesTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        String verwerking1Text = favorietTrajectPlaatsLabel.getText().replace("<html>Traject van: <b style='color:999999'>", "");
                                        String verwerking2Text = verwerking1Text.replace("</b> naar: <b style='color:999999'>", ";");
                                        String verwerking3Text = verwerking2Text.replace("</b></html>", "");
                                        String[] plaatsen = verwerking3Text.split(";");
                                        vertrekText.setText(plaatsen[0]);
                                        aankomstText.setText(plaatsen[1]);
                                        tijdstipText.setText(String.valueOf(profile.getFavorietTrajectMetroTijd().get(q)));
                                        jScrollPane1.setVisible(true);
                                        kaartLabel.setVisible(true);
                                        favorietTrajectPanel.setVisible(false);
                                        favorieteTrajectenPanel.setVisible(false);
                                        favorieteTrajectenPanel.removeAll();

                                        bevestigingButtonActionPerformed(evt);
                                    }
                                });

                                verwijderTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        switch(verwijderTrajectButton.getText())
                                        {
                                            case"Verwijderen":

                                                String verwerking1Text = favorietTrajectPlaatsLabel.getText().replace("<html>Traject van: <b style='color:999999'>", "");
                                                String verwerking2Text = verwerking1Text.replace("</b> naar: <b style='color:999999'>", ";");
                                                String verwerking3Text = verwerking2Text.replace("</b></html>", "");
                                                String[] plaatsen = verwerking3Text.split(";");

                                                for (MetroTijd metroTijd : JSONMetroReader.getMetroTijd())
                                                {
                                                    if (metroTijd.getStationVertrek().toLowerCase().equals(plaatsen[0].toLowerCase())&&metroTijd.getStationAankomst().toLowerCase().equals(plaatsen[1].toLowerCase()))
                                                    {
                                                        for (int i = 0; i < profile.getFavorietTrajectMetro().size(); i++)
                                                        {
                                                            if (profile.getFavorietTrajectMetro().get(i).equals(Integer.valueOf(metroTijd.getTraject())))
                                                            {
                                                                profile.removeFavorietTrajectMetro(i);

                                                                // leegmaken van de panel
                                                                trajectPanel.removeAll();
                                                                trajectPanel.repaint();
                                                            }
                                                        }
                                                    }
                                                }
                                                break;

                                            case"Ontdoen":

                                                trajectPanel.add(favorietTrajectTijdLabel);
                                                trajectPanel.add(favorietTrajectPlaatsLabel);
                                                trajectPanel.remove(bewerkAankomstLabel);
                                                trajectPanel.remove(bewerkAankomstTextField);
                                                trajectPanel.remove(bewerkVertrekLabel);
                                                trajectPanel.remove(bewerkVertrekTextField);
                                                trajectPanel.remove(bewerkTijdLabel);
                                                trajectPanel.remove(bewerkTijdTextField);
                                                trajectPanel.remove(bewerkTijdLabel2);
                                                trajectPanel.repaint();

                                                verwijderTrajectButton.setText("Verwijderen");
                                                bewerkTrajectButton.setText("Bewerken");

                                                break;
                                        }


                                    }
                                });

                                bewerkTrajectButton.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {

                                        switch(bewerkTrajectButton.getText())
                                        {
                                            case"Bewerken":

                                                trajectPanel.remove(favorietTrajectTijdLabel);
                                                trajectPanel.remove(favorietTrajectPlaatsLabel);
                                                trajectPanel.add(bewerkAankomstLabel);
                                                trajectPanel.add(bewerkAankomstTextField);
                                                trajectPanel.add(bewerkVertrekLabel);
                                                trajectPanel.add(bewerkVertrekTextField);
                                                trajectPanel.add(bewerkTijdLabel);
                                                trajectPanel.add(bewerkTijdTextField);
                                                trajectPanel.add(bewerkTijdLabel2);
                                                trajectPanel.repaint();

                                                bewerkTrajectButton.setText("Opslaan");
                                                verwijderTrajectButton.setText("Ontdoen");

                                                break;

                                            case"Opslaan":

                                                // gewijzigde informatie opslaan
                                                // for (int t = 0; t < profile.getFavorietTrajectMetro().size(); t++)
                                                // {
                                                for (int b = 0; b < JSONMetroReader.getMetroTijd().size(); b++)
                                                {
                                                    if (JSONMetroReader.getMetroTijd().get(b).getStationVertrek().toLowerCase().equals(bewerkVertrekTextField.getText().toLowerCase())&&JSONMetroReader.getMetroTijd().get(b).getStationAankomst().toLowerCase().equals(bewerkAankomstTextField.getText().toLowerCase()))
                                                    {
                                                        for (int i = 0; i < profile.getFavorietTrajectMetro().size(); i++)
                                                        {
                                                            if (profile.getFavorietTrajectMetro().get(i).equals(Integer.valueOf(metroTijd.getTraject())))
                                                            {
                                                                profile.removeFavorietTrajectMetro(i);

                                                                // nieuwe check voor invoeren nieuw favoriet traject
                                                                for (MetroTijd metroTijd2 : JSONMetroReader.getMetroTijd())
                                                                {
                                                                    if (metroTijd2.getStationVertrek().toLowerCase().equals(bewerkVertrekTextField.getText().toLowerCase())
                                                                            && metroTijd2.getStationAankomst().toLowerCase().equals(bewerkAankomstTextField.getText().toLowerCase()))
                                                                    {

                                                                        profile.setFavorietTrajectMetro(Integer.valueOf(metroTijd2.getTraject()), LocalTime.parse(bewerkTijdTextField.getText()));
                                                                        System.out.println("nieuw traject toegevoegd");

                                                                        // labels updaten
                                                                        favorietTrajectTijdLabel.setText("<html>Vanaf <b style='color:999999'>"+bewerkTijdTextField.getText()+" uur</html>");
                                                                        favorietTrajectPlaatsLabel.setText("<html>Traject van: <b style='color:999999'>"+metroTijd2.getStationVertrek()+"</b> naar: <b style='color:999999'>"+metroTijd2.getStationAankomst()+"</b></html>");

                                                                        // refreshen van de panel
                                                                        trajectPanel.remove(bewerkAankomstLabel);
                                                                        trajectPanel.remove(bewerkAankomstTextField);
                                                                        trajectPanel.remove(bewerkVertrekLabel);
                                                                        trajectPanel.remove(bewerkVertrekTextField);
                                                                        trajectPanel.remove(bewerkTijdLabel);
                                                                        trajectPanel.remove(bewerkTijdTextField);
                                                                        trajectPanel.remove(bewerkTijdLabel2);
                                                                        trajectPanel.add(favorietTrajectTijdLabel);
                                                                        trajectPanel.add(favorietTrajectPlaatsLabel);
                                                                        trajectPanel.repaint();

                                                                        bewerkTrajectButton.setText("Bewerken");
                                                                        verwijderTrajectButton.setText("Verwijderen");

                                                                        break;
                                                                    }

                                                                }
                                                                break;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        System.out.println("Geen overeenkomst.\nAantal gecheckte metro modellen: "+b);
                                                    }
                                                    if (b==JSONMetroReader.getMetroTijd().size()-1)
                                                    {
                                                        System.out.println("definitief geen match gevonden!");
                                                        JOptionPane.showMessageDialog(mainPanel, "Dit traject staat niet in ons systeem. (Controleer mogelijk op spelfouten)",
                                                        "Verkeerde invoer", JOptionPane.WARNING_MESSAGE);

                                                        bewerkVertrekTextField.setText(metroTijd.getStationVertrek());
                                                        bewerkAankomstTextField.setText(metroTijd.getStationAankomst());

                                                        break;
                                                    }
                                                }
                                            break;
                                        }

                                    }
                                });

                                break;
                            }

                        }
                        break;
                    }

                }

                // 1 iteratie optellen
                t++;
            }

            if (t>5)
            {
                favorieteTrajectenPanel.setLayout(new GridLayout(5, 2, 40, 40));
            } else if (t<=4)
            {
                favorieteTrajectenPanel.setLayout(new GridLayout(5, 1, 40, 40));
            }


        }

        return favorieteTrajectenPanel;
    }



    private void closeButtonMouseClicked(java.awt.event.MouseEvent evt) {
        System.exit(0);
    }

    private void minButtonMouseClicked(java.awt.event.MouseEvent evt) {
        this.setState(JFrame.ICONIFIED);
    }

    private void treinButtonMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==treinButton) {
            selectTrein.setVisible(true);
            selectTram.setVisible(false);
            selectMetro.setVisible(false);
            selectBus.setVisible(false);
        }
    }

    private void tramButtonMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==tramButton) {
            selectTrein.setVisible(false);
            selectTram.setVisible(true);
            selectMetro.setVisible(false);
            selectBus.setVisible(false);
        }
    }

    private void metroButtonMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==metroButton) {
            selectTrein.setVisible(false);
            selectTram.setVisible(false);
            selectMetro.setVisible(true);
            selectBus.setVisible(false);
        }
    }

    private void busButtonMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==busButton) {
            selectTrein.setVisible(false);
            selectTram.setVisible(false);
            selectMetro.setVisible(false);
            selectBus.setVisible(true);
        }
    }

    private void treinButtonMouseEntered(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==treinButton) {
            treinButton.setForeground(new Color(255,255,255));
        }
    }

    private void treinButtonMouseExited(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==treinButton) {
            treinButton.setForeground(new Color(167,167,167));
        }
    }

    private void tramButtonMouseEntered(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==tramButton) {
            tramButton.setForeground(new Color(255,255,255));
        }
    }

    private void tramButtonMouseExited(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==tramButton) {
            tramButton.setForeground(new Color(167,167,167));
        }
    }

    private void metroButtonMouseEntered(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==metroButton) {
            metroButton.setForeground(new Color(255,255,255));
        }
    }

    private void metroButtonMouseExited(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==metroButton) {
            metroButton.setForeground(new Color(167,167,167));
        }
    }

    private void busButtonMouseEntered(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==busButton) {
            busButton.setForeground(new Color(255,255,255));
        }
    }

    private void busButtonMouseExited(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==busButton) {
            busButton.setForeground(new Color(167,167,167));
        }
    }

    private void dragbarMouseDragged(java.awt.event.MouseEvent evt) {
        int valueX = evt.getXOnScreen();
        int valueY = evt.getYOnScreen();
        
        this.setLocation(valueX-mouseX, valueY-mouseY);
    }

    private void dragbarMousePressed(java.awt.event.MouseEvent evt) {
        mouseX = evt.getX();
        mouseY = evt.getY();
    }

    private void bevestigingButtonActionPerformed(java.awt.event.ActionEvent evt) {
        DefaultTableModel defaultTableModel = new DefaultTableModel(new Object[][]{}, new String[]{
                languagesBundle.getString("defaultTableModelVerterkStation"),
                languagesBundle.getString("defaultTableModelPerron"),
                languagesBundle.getString("defaultTableModelVertrekTijd"),
                languagesBundle.getString("defaultTableModelAankomstStation"),
                languagesBundle.getString("defaultTableModelAankomsttijd"),
                languagesBundle.getString("defaultTableModelAfstand")});
        Date userDate = new Date();
        LocalTime time = LocalTime.parse(tijdstipText.getText());
        SimpleDateFormat simpleFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String userDateText = datumText.getText() + " " + tijdstipText.getText();
        try
        {
            userDate = simpleFormat.parse(userDateText);
            if(userDate.before(new Date(120, 3, 27)))
            {
                JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonDatumDialog"),
                languagesBundle.getString("foutmelding"), JOptionPane.WARNING_MESSAGE);
            }
            else if (selectTram.isVisible())
            {
                for (TramTijd tramTijd : JSONTramReader.getTramTijd())
                {
                    if (tramTijd.getStationVertrek().toLowerCase().equals(vertrekText.getText().toLowerCase())
                            && tramTijd.getStationAankomst().toLowerCase().equals(aankomstText.getText().toLowerCase()))
                    {
                        for (int l = 0 ; l < (tramTijd.getReisTijden().size()/2); l++)
                        {
                            if (time.isBefore(tramTijd.getReisTijden().get(l)))
                            {
                                profile.setDetailTraject(tramTijd.getTramNummer());
                                defaultTableModel.addRow(new Object[]
                                        {
                                                tramTijd.getStationVertrek(),
                                                tramTijd.getTramHalte(),
                                                (tramTijd.getReisTijden().get(l)),
                                                tramTijd.getStationAankomst(),
                                                (tramTijd.getReisTijden().get(l+1)),
                                                tramTijd.getReisAfstand()
                                        });
                            }
                        }

                    }
                }

                switch (vertrekText.getText().toLowerCase())
                {
                    case "den haag centraal":
                        if (aankomstText.getText().toLowerCase().equals("scheveningen"))
                        {
                            kaartLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/TramreisDENHAAG-SCH.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonTramDialog"), languagesBundle.getString("berichtmelding"),
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    case "utrecht centraal":
                        if (aankomstText.getText().toLowerCase().equals("utrecht science park"))
                        {
                            kaartLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/TramreisUTR-SCI.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonTramDialog"), languagesBundle.getString("berichtmelding"),
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    case "amsterdam centraal":
                        if (aankomstText.getText().toLowerCase().equals("amsterdam zuid"))
                        {
                            kaartLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/TramreisAMS-AMSZUID.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonTramDialog"), languagesBundle.getString("berichtmelding"),
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    default:
                        JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonTramDialog"),
                                languagesBundle.getString("berichtmelding"), JOptionPane.INFORMATION_MESSAGE);
                        break;
                }
            }
            else if (selectBus.isVisible())
            {
                for (BusTijd busTijd : JSONBusReader.getBusTijd())
                {
                    if (busTijd.getStationVertrek().toLowerCase().equals(vertrekText.getText().toLowerCase())
                            && busTijd.getStationAankomst().toLowerCase().equals(aankomstText.getText().toLowerCase()))
                    {
                        for (int j = 0 ; j < (busTijd.getReisTijden().size()/2); j++)
                        {
                            if (time.isBefore(busTijd.getReisTijden().get(j)))
                            {
                                profile.setDetailTraject(busTijd.getBusNummer());
                                defaultTableModel.addRow(new Object[]
                                        {
                                                busTijd.getStationVertrek(),
                                                busTijd.getBushalte(),
                                                busTijd.getReisTijden().get(j),
                                                busTijd.getStationAankomst(),
                                                busTijd.getReisTijden().get(j+1),
                                                busTijd.getReisAfstand()
                                        });
                            }
                        }

                    }
                }

                switch (vertrekText.getText().toLowerCase())
                {
                    case "amersfoort centraal":
                        if (aankomstText.getText().toLowerCase().equals("huizen centraal"))
                        {
                            kaartLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/BusreisAMER-HUIZEN.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonBusDialog"), languagesBundle.getString("berichtmelding"),
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    case "utrecht centraal":
                        if (aankomstText.getText().toLowerCase().equals("gouda centraal"))
                        {
                            kaartLabel.setIcon(new ImageIcon("src/photoLib/BusreisUTR-GOUDA.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonBusDialog"), languagesBundle.getString("berichtmelding"),
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    case "amsterdam centraal":
                        if (aankomstText.getText().toLowerCase().equals("haarlem centraal"))
                        {
                            kaartLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/BusreisAMS-HAAR.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonBusDialog"), languagesBundle.getString("berichtmelding"),
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    default:
                        JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonBusDialog"),
                                languagesBundle.getString("berichtmelding"),
                                JOptionPane.INFORMATION_MESSAGE);
                        break;
                }
            }
            else if (selectTrein.isVisible())
            {
                for (TreinTijd treinTijd : JSONTreinReader.getTreinTijd())
                {
                    if (treinTijd.getStationVertrek().toLowerCase().equals(vertrekText.getText().toLowerCase())
                            && treinTijd.getStationAankomst().toLowerCase().equals(aankomstText.getText().toLowerCase()))
                    {
                        for (int i = 0 ; i < (treinTijd.getReisTijden().size()/2); i++)
                        {
                            if (time.isBefore(treinTijd.getReisTijden().get(i)))
                            {
                                profile.setDetailTraject(treinTijd.getTreinNummer());
                                defaultTableModel.addRow(new Object[]
                                        {
                                                treinTijd.getStationVertrek(),
                                                treinTijd.getPerron(),
                                                treinTijd.getReisTijden().get(i),
                                                treinTijd.getStationAankomst(),
                                                treinTijd.getReisTijden().get(i+1),
                                                treinTijd.getReisAfstand()
                                        });

                            }
                        }
                    }
                }

                switch (vertrekText.getText().toLowerCase())
                {
                    case "amersfoort centraal":
                        if (aankomstText.getText().toLowerCase().equals("veenendaal centraal"))
                        {
                            kaartLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/TreinreisAMER-VEEN.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, "Er rijdt geen trein naar het door u " +
                                            "opgegeven station. Kijk of u het aankomstpunt goed hebt gespelt.", languagesBundle.getString("berichtmelding"),
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    case "utrecht centraal":
                        if (aankomstText.getText().toLowerCase().equals("amersfoort centraal"))
                        {
                            kaartLabel.setIcon(new ImageIcon("src/photoLib/TreinReisUTR-AMER.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonTreinDialog"), languagesBundle.getString("berichtmelding"),
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    case "amsterdam centraal":
                        if (aankomstText.getText().toLowerCase().equals("amersfoort centraal"))
                        {
                            kaartLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/TreinreisAMST-AMER.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonTreinDialog"), languagesBundle.getString("berichtmelding"),
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    default:
                        JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonTreinDialog"),
                                languagesBundle.getString("berichtmelding"), JOptionPane.INFORMATION_MESSAGE);
                        break;
                }
            }
            else if (selectMetro.isVisible())
            {
                for (MetroTijd metroTijd : JSONMetroReader.getMetroTijd())
                {
                    if (metroTijd.getStationVertrek().toLowerCase().equals(vertrekText.getText().toLowerCase())
                            && metroTijd.getStationAankomst().toLowerCase().equals(aankomstText.getText().toLowerCase()))
                    {
                        for (int i = 0 ; i < (metroTijd.getReisTijden().size()/2); i++)
                        {
                            if (time.isBefore(metroTijd.getReisTijden().get(i)))
                            {
                                profile.setDetailTraject(metroTijd.getMetroNummer());
                                defaultTableModel.addRow(new Object[]
                                        {
                                                metroTijd.getStationVertrek(),
                                                metroTijd.getMetroHalte(),
                                                 (metroTijd.getReisTijden().get(i)),
                                                metroTijd.getStationAankomst(),
                                                 (metroTijd.getReisTijden().get(i+1)),
                                                metroTijd.getReisAfstand()
                                        });
                            }
                        }

                    }
                }

                switch (vertrekText.getText().toLowerCase())
                {
                    case "rotterdam centraal":
                        if (aankomstText.getText().toLowerCase().equals("den haag centraal"))
                        {
                            kaartLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/MetroreisROT-DENHAAG.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, "Er rijdt geen metro naar het door u " +
                                            "opgegeven station. Kijk of u het aankomstpunt goed hebt gespelt.", "Bericht",
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    case "amsterdam centraal":
                        if (aankomstText.getText().toLowerCase().equals("diemen centraal"))
                        {
                            kaartLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/MetroreisAMS-DIE.png"));
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonMetroDialog"), languagesBundle.getString("berichtmelding"),
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        break;
                    default:
                        JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("bevestigingButtonMetroDialog"),
                                languagesBundle.getString("berichtmelding"), JOptionPane.INFORMATION_MESSAGE);
                        break;
                }
            }

            jTable1.setModel(defaultTableModel);
            jScrollPane1.setVisible(true);
            kaartLabel.setVisible(true);
            favorietTrajectPanel.setVisible(false);
            favorieteTrajectenPanel.setVisible(false);
            favorieteTrajectenPanel.removeAll();
        }
        catch (ParseException PaEx)
        {
            JOptionPane.showMessageDialog(mainPanel,languagesBundle.getString("bevestigingButtonErrorDialog"),
                    languagesBundle.getString("errormelding"), JOptionPane.ERROR_MESSAGE);
        }
    }

    private void trajectopslaanButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Date userDate = new Date();
        LocalTime userTime = LocalTime.parse(tijdstipText.getText());
        SimpleDateFormat simpleFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String userDateText = datumText.getText() + " " + tijdstipText.getText();

        try
        {
            userDate = simpleFormat.parse(userDateText);
        }
        catch(ParseException e)
        {
            System.out.println(e);
        }

        
        if (selectBus.isVisible())
        {
            for (BusTijd busTijd : JSONBusReader.getBusTijd())
            {
                if (busTijd.getStationVertrek().toLowerCase().equals(vertrekText.getText())
                        && busTijd.getStationAankomst().toLowerCase().equals(aankomstText.getText()))
                {
                    try
                    {
                        if (profile.getFavorietTrajectBus().size()>0)
                        {
                            for (Integer favTraject = 0; favTraject < profile.getFavorietTrajectBus().size(); favTraject++)
                            {
                                // als traject reeds in favorieten staat check
                                if (Integer.valueOf(busTijd.getTraject()).equals(profile.getFavorietTrajectBus().get(favTraject)))
                                {
                                    System.out.println("Dit traject heeft u al opgeslagen!");
                                    break;
                                } else if (favTraject == profile.getFavorietTrajectBus().size()-1)
                                {
                                    profile.setFavorietTrajectBus(Integer.valueOf(busTijd.getTraject()), userTime);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            profile.setFavorietTrajectBus(Integer.valueOf(busTijd.getTraject()), userTime);
                        }
                        // voor testen:
                        System.out.println("Favoriete trajecten bus:");
                        for (Integer favoTraject : profile.getFavorietTrajectBus())
                        {
                            System.out.print(" "+favoTraject);
                        }
                        System.out.println(profile.getFavorietTrajectBusTijd());
                        // einde testen
                        break;
                    }
                    catch (NullPointerException e)
                    {
                        break;
                    }
                }
            }
        }

        else if (selectTram.isVisible())
        {
            for (TramTijd tramTijd : JSONTramReader.getTramTijd())
            {
                if (tramTijd.getStationVertrek().toLowerCase().equals(vertrekText.getText().toLowerCase())
                        && tramTijd.getStationAankomst().toLowerCase().equals(aankomstText.getText().toLowerCase()))
                {
                    try
                    {
                        if (profile.getFavorietTrajectTram().size()>0)
                        {
                            for (Integer favTraject = 0; favTraject < profile.getFavorietTrajectTram().size(); favTraject++)
                            {
                                // als traject reeds in favorieten staat check
                                if (Integer.valueOf(tramTijd.getTraject()).equals(profile.getFavorietTrajectTram().get(favTraject)))
                                {
                                    System.out.println("Dit traject heeft u al opgeslagen!");
                                    break;
                                } else if (favTraject == profile.getFavorietTrajectTram().size()-1)
                                {
                                    profile.setFavorietTrajectTram(Integer.valueOf(tramTijd.getTraject()), userTime);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            profile.setFavorietTrajectTram(Integer.valueOf(tramTijd.getTraject()), userTime);
                        }
                        // voor testen:
                        System.out.println("Favoriete trajecten tram:");
                        for (Integer favoTraject : profile.getFavorietTrajectTram())
                        {
                            System.out.print(" "+favoTraject);
                        }
                        System.out.println();
                        // einde testen
                        break;
                    }
                    catch (NullPointerException e)
                    {
                        break;
                    }
                }
            }
        }

        else if (selectTrein.isVisible())
        {
            for (TreinTijd treinTijd : JSONTreinReader.getTreinTijd())
            {
                if (treinTijd.getStationVertrek().toLowerCase().equals(vertrekText.getText().toLowerCase())
                        && treinTijd.getStationAankomst().toLowerCase().equals(aankomstText.getText().toLowerCase()))
                {
                    try
                    {
                        if (profile.getFavorietTrajectTrein().size()>0)
                        {
                            for (Integer favTraject = 0; favTraject < profile.getFavorietTrajectTrein().size(); favTraject++)
                            {
                                // als traject reeds in favorieten staat check
                                if (Integer.valueOf(treinTijd.getTraject()).equals(profile.getFavorietTrajectTrein().get(favTraject)))
                                {
                                    System.out.println("Dit traject heeft u al opgeslagen!");
                                    break;
                                } else if (favTraject == profile.getFavorietTrajectTrein().size()-1)
                                {
                                    profile.setFavorietTrajectTrein(Integer.valueOf(treinTijd.getTraject()), userTime);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            profile.setFavorietTrajectTrein(Integer.valueOf(treinTijd.getTraject()), userTime);
                        }
                        // voor testen:
                        System.out.println("Favoriete trajecten trein:");
                        for (Integer favoTraject : profile.getFavorietTrajectTrein())
                        {
                            System.out.print(" "+favoTraject);
                        }
                        System.out.println();
                        // einde testen
                        break;
                    }
                    catch (NullPointerException e)
                    {
                        break;
                    }
                }
            }
        }

        else if (selectMetro.isVisible())
        {
            for (MetroTijd metroTijd : JSONMetroReader.getMetroTijd())
            {
                if (metroTijd.getStationVertrek().toLowerCase().equals(vertrekText.getText().toLowerCase())
                        && metroTijd.getStationAankomst().toLowerCase().equals(aankomstText.getText().toLowerCase()))
                {
                    try
                    {
                        if (profile.getFavorietTrajectMetro().size()>0)
                        {
                            for (Integer favTraject = 0; favTraject < profile.getFavorietTrajectMetro().size(); favTraject++)
                            {
                                // als traject reeds in favorieten staat check
                                if (Integer.valueOf(metroTijd.getTraject()).equals(profile.getFavorietTrajectMetro().get(favTraject)))
                                {
                                    System.out.println("Dit traject heeft u al opgeslagen!");
                                    break;
                                } else if (favTraject == profile.getFavorietTrajectMetro().size()-1)
                                {
                                    profile.setFavorietTrajectMetro(Integer.valueOf(metroTijd.getTraject()), userTime);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            profile.setFavorietTrajectMetro(Integer.valueOf(metroTijd.getTraject()), userTime);
                        }
                        // voor testen:
                        System.out.println("Favoriete trajecten metro:");
                        for (Integer favoTraject : profile.getFavorietTrajectMetro())
                        {
                            System.out.print(" "+favoTraject);
                        }
                        System.out.println();
                        // einde testen
                        break;
                    }
                    catch (NullPointerException e)
                    {
                        break;
                    }
                }
            }
        }
    }

    private void favorietTrajectButtonActionPerformed(java.awt.event.ActionEvent evt) {

        // remove en add functie voor refreshen van pagina
        if (jScrollPane1.isVisible())
        {
            jScrollPane1.setVisible(false);
            kaartLabel.setVisible(false);
            favorietTrajectPanel.add(getTrajectenPanel());
            favorietTrajectPanel.setVisible(true);
            favorieteTrajectenPanel.setVisible(true);
        } else
        {
            jScrollPane1.setVisible(true);
            kaartLabel.setVisible(true);
            favorietTrajectPanel.setVisible(false);
            favorieteTrajectenPanel.setVisible(false);
            favorieteTrajectenPanel.removeAll();
        }

    }

    private void datumTextActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void vertrekTextActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void aankomstTextActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void tijdstipTextActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void userIconMouseEntered(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==userIcon) {
            userIcon.setIcon(new javax.swing.ImageIcon("src/photoLib/icons8-test-account-48-3.png"));
            usernameLabel.setForeground(new Color(255,255,255));
        }
    }

    private void userIconMouseExited(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==userIcon) {
            userIcon.setIcon(new javax.swing.ImageIcon("src/photoLib/icons8-test-account-48-4.png"));
            usernameLabel.setForeground(new Color(167,167,167));
        }
    }

    private void registerLoginButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource()==registerLoginButton) {
            loginPanel.setVisible(false);
            registerPanel.setVisible(true);
            usernameText.setText("");
            passwordText.setText("");
            Thread th2 = new Thread() {
                @Override
                public void run() {
                try {
                    for ( int i = 1025; i < 1375; i++){
                        Thread.sleep(1);
                        languagesPanel.setLocation(i, 100);
                    }
                }
                catch (Exception e){
                    JOptionPane.showMessageDialog(null,e);
                }
            }};th2.start();
            languageValue = 0;
        }
    }

    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource()==loginButton) {

            String keyPassword1 = profile1.getPassword();
            String keyPassword2 = profile2.getPassword();
            String keyPassword3 = profile3.getPassword();

            if (usernameText.getText().equals(profile1.getUserName())
                    && (String.valueOf(passwordText.getPassword()).equals(keyPassword1))
                    || usernameText.getText().equals(profile2.getUserName())
                    && (String.valueOf(passwordText.getPassword()).equals(keyPassword2))
                    || usernameText.getText().equals(profile3.getUserName())
                    && (String.valueOf(passwordText.getPassword()).equals(keyPassword3)))
            {
                if (usernameText.getText().equals(profile1.getUserName()))
                {
                    profile = profile1;
                    usernameLabel.setText(profile.getUserName());
                    vertrekText.setText(profile.getHomeStation());
                    usernameProfielLabel2.setText(profile.getUserName());
                    stationProfielLabel2.setText(profile.getHomeStation());
                    usernameProfielWijzigText.setText(profile.getUserName());
                    stationWijzingText.setText(profile.getHomeStation());
                }
                else if (usernameText.getText().equals(profile2.getUserName()))
                {
                    profile = profile2;
                    usernameLabel.setText(profile.getUserName());
                    vertrekText.setText(profile.getHomeStation());
                    usernameProfielLabel2.setText(profile.getUserName());
                    stationProfielLabel2.setText(profile.getHomeStation());
                    usernameProfielWijzigText.setText(profile.getUserName());
                    stationWijzingText.setText(profile.getHomeStation());
                }
                else if (usernameText.getText().equals(profile3.getUserName()))
                {
                    profile = profile3;
                    usernameLabel.setText(profile.getUserName());
                    vertrekText.setText(profile.getHomeStation());
                    usernameProfielLabel2.setText(profile.getUserName());
                    stationProfielLabel2.setText(profile.getHomeStation());
                    usernameProfielWijzigText.setText(profile.getUserName());
                    stationWijzingText.setText(profile.getHomeStation());
                }
                System.out.println("Succes! You're logged in.");
                mainPanel.setVisible(true);
                loginPanel.setVisible(false);
                messageLabel.setVisible(false);
                messageLabel2.setVisible(false);
                usernameLabel1.setForeground(new Color(255, 255, 255));
                passwordLabel.setForeground(new Color(255, 255, 255));
                usernameText.setText("");
                passwordText.setText("");
                Thread th2 = new Thread() {
                    @Override
                    public void run() {
                    try {
                        for ( int i = 1025; i < 1375; i++){
                            Thread.sleep(1);
                            languagesPanel.setLocation(i, 100);
                        }
                    }
                    catch (Exception e){
                        JOptionPane.showMessageDialog(null,e);
                    }
                }};th2.start();
                languageValue = 0;
            }
            else
            {
                messageLabel.setForeground(new Color(255, 0, 51));
                messageLabel2.setForeground(new Color(255, 0, 51));
                messageLabel.setText(languagesBundle.getString("messageLabel"));
                messageLabel2.setText(languagesBundle.getString("messageLabel2"));

                messageLabel.setVisible(true);
                messageLabel2.setVisible(true);
                usernameLabel1.setForeground(new Color(255, 0, 51));
                passwordLabel.setForeground(new Color(255, 0, 51));

                //Set signInPanel visible to false and then true to fix a visual bug.
                signInPanel.setVisible(false);
                signInPanel.setVisible(true);
                System.out.println("Couldn't find your account. Please make sure both your username and password are correct.");

                //Test of de twee arrays gelijk zijn.
                System.out.println(passwordText.getPassword());
                if (usernameText.getText() == profile1.getUserName())
                {
                    System.out.println(profile1.getPassword());
                }
                if (usernameText.getText() == profile2.getUserName())
                {
                    System.out.println(profile2.getPassword());
                }
                if (usernameText.getText() == profile3.getUserName())
                {
                    System.out.println(profile3.getPassword());
                }
            }
        }
    }

    private void usernameTextActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void minButton1MouseClicked(java.awt.event.MouseEvent evt) {
        this.setState(JFrame.ICONIFIED);
    }

    private void closeButton1MouseClicked(java.awt.event.MouseEvent evt) {
        System.exit(0);
    }

    private void dragbar1MouseDragged(java.awt.event.MouseEvent evt) {
        int valueX = evt.getXOnScreen();
        int valueY = evt.getYOnScreen();

        this.setLocation(valueX-mouseX, valueY-mouseY);
    }

    private void dragbar1MousePressed(java.awt.event.MouseEvent evt) {
        mouseX = evt.getX();
        mouseY = evt.getY();
    }

    private void userIconMousePressed(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==userIcon) {
            mainPanel.setVisible(false);
            profielPanel.setVisible(true);
        }
    }

    private void minButton2MouseClicked(java.awt.event.MouseEvent evt) {
        this.setState(JFrame.ICONIFIED);
    }

    private void closeButton2MouseClicked(java.awt.event.MouseEvent evt) {
        System.exit(0);
    }

    private void dragbar2MouseDragged(java.awt.event.MouseEvent evt) {
        int valueX = evt.getXOnScreen();
        int valueY = evt.getYOnScreen();

        this.setLocation(valueX-mouseX, valueY-mouseY);
    }

    private void dragbar2MousePressed(java.awt.event.MouseEvent evt) {
        mouseX = evt.getX();
        mouseY = evt.getY();
    }

    private void usernameProfielWijzigTextActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void stationWijzingTextActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void annulerenButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource()==annulerenButton) {
            usernameProfielWijzigText.setText(profile.getUserName());
            stationWijzingText.setText(profile.getHomeStation());
            wijzigPanel.setVisible(false);
            profielPanel.setVisible(true);
        }
    }

    private void opslaanButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource()==opslaanButton) {
            if (usernameProfielWijzigText.getText().length() > 3)
            {
                profile.setUserName(usernameProfielWijzigText.getText());
                profile.setHomeStation(stationWijzingText.getText());
                usernameProfielLabel2.setText(profile.getUserName());
                stationProfielLabel2.setText(profile.getHomeStation());
                usernameLabel.setText(profile.getUserName());
                vertrekText.setText(profile.getHomeStation());
                wijzigPanel.setVisible(false);
                profielPanel.setVisible(true);
            }
            else
            {
                JOptionPane.showMessageDialog(mainPanel, languagesBundle.getString("opslaanButtonDialog"),
                        languagesBundle.getString("foutmelding"), JOptionPane.WARNING_MESSAGE);
            }

        }
    }

    private void minButton3MouseClicked(java.awt.event.MouseEvent evt) {
        this.setState(JFrame.ICONIFIED);
    }

    private void closeButton3MouseClicked(java.awt.event.MouseEvent evt) {
        System.exit(0);
    }

    private void dragbar3MouseDragged(java.awt.event.MouseEvent evt) {
        int valueX = evt.getXOnScreen();
        int valueY = evt.getYOnScreen();

        this.setLocation(valueX-mouseX, valueY-mouseY);
    }

    private void dragbar3MousePressed(java.awt.event.MouseEvent evt) {
        mouseX = evt.getX();
        mouseY = evt.getY();
    }

    private void wijzigenButtonActionPerformed(java.awt.event.ActionEvent evt) {
        wijzigPanel.setVisible(true);
        profielPanel.setVisible(false);
    }

    private void logUitButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource()==logUitButton) {
            loginPanel.setVisible(true);
            profielPanel.setVisible(false);
        }
    }

    private void terugButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource()==terugButton) {
            profielPanel.setVisible(false);
            mainPanel.setVisible(true);
        }
    }

    private void usernameRegisterTextActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void stationRegisterTextActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void cancelRegisterButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource()==cancelRegisterButton) {
            registerPanel.setVisible(false);
            loginPanel.setVisible(true);
            usernameRegisterText.setText("");
            stationRegisterText.setText("");
            passwordRegisterText.setText("");
            retypePasswordRegisterText.setText("");
            messageLabel.setVisible(false);
            messageLabel2.setVisible(false);
            usernameLabel1.setForeground(new Color(255, 255, 255));
            passwordLabel.setForeground(new Color(255, 255, 255));
        }
    }

    private void registerRegisterButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource()==registerRegisterButton)
        {
            profile1.setUserName(usernameRegisterText.getText());
            profile1.setHomeStation(stationRegisterText.getText());
            if (String.valueOf(passwordRegisterText.getPassword()).equals(String.valueOf((retypePasswordRegisterText.getPassword()))))
            {
                System.out.println("You have been registered!");
                profile1.setPassword(String.valueOf(passwordRegisterText.getPassword()));
                profile = profile1;
                usernameLabel.setText(profile.getUserName());
                vertrekText.setText(profile.getHomeStation());
                usernameProfielLabel2.setText(profile.getUserName());
                stationProfielLabel2.setText(profile.getHomeStation());
                usernameProfielWijzigText.setText(profile.getUserName());
                stationWijzingText.setText(profile.getHomeStation());
            }
            registerPanel.setVisible(false);
            loginPanel.setVisible(true);
            usernameRegisterText.setText("");
            stationRegisterText.setText("");
            passwordRegisterText.setText("");
            retypePasswordRegisterText.setText("");

            messageLabel.setForeground(new Color(0, 255, 51));
            messageLabel2.setForeground(new Color(0, 255, 51));
            usernameLabel1.setForeground(new Color(255, 255, 255));
            passwordLabel.setForeground(new Color(255, 255, 255));
            messageLabel.setText(languagesBundle.getString("messageLabelRegister"));
            messageLabel2.setText(languagesBundle.getString("messageLabelRegister2"));
            messageLabel.setVisible(true);
            messageLabel2.setVisible(true);
        }
    }

    private void minButton4MouseClicked(java.awt.event.MouseEvent evt) {
        this.setState(JFrame.ICONIFIED);
    }

    private void closeButton4MouseClicked(java.awt.event.MouseEvent evt) {
        System.exit(0);
    }

    private void dragbar4MouseDragged(java.awt.event.MouseEvent evt) {
        int valueX = evt.getXOnScreen();
        int valueY = evt.getYOnScreen();

        this.setLocation(valueX-mouseX, valueY-mouseY);
    }

    private void dragbar4MousePressed(java.awt.event.MouseEvent evt) {
        mouseX = evt.getX();
        mouseY = evt.getY();
    }

    private void languageLabelMouseEntered(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==languageLabel) {
            languageLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/settingsSelected.png"));
            languageLabel.setForeground(new Color(255,255,255));
        }
    }

    private void languageLabelMouseExited(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==languageLabel) {
            languageLabel.setIcon(new javax.swing.ImageIcon("src/photoLib/settings.png"));
            languageLabel.setForeground(new Color(167,167,167));
        }
    }

    private void languageLabelMousePressed(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==languageLabel) {
            languagesPanel.setVisible(true);
            if (languageValue == 0)
                {
                    Thread th = new Thread() {
                        @Override
                        public void run() {
                        try {
                            for ( int i = 1375; i > 1025; i--){
                                Thread.sleep(1);
                                languagesPanel.setLocation(i, 100);
                            }
                        } catch (Exception e){
                            JOptionPane.showMessageDialog(null,e);
                        }
                    }
                };th.start();
                languageValue = 1;
            }
            else if (languageValue == 1)
            {
                Thread th2 = new Thread() {
                    @Override
                    public void run() {
                    try {
                        for ( int i = 1025; i < 1375; i++){
                            Thread.sleep(1);
                            languagesPanel.setLocation(i, 100);
                        }
                    } catch (Exception e){
                        JOptionPane.showMessageDialog(null,e);
                    }
                }
            };th2.start();
            languageValue = 0;
        }
        }
    }

    private void englishLabelMouseEntered(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==englishLabel) {
            englishLabel.setForeground(new Color(255,255,255));
            languagesPanel.setVisible(false);
            languagesPanel.setVisible(true);

        }
    }

    private void englishLabelMouseExited(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==englishLabel) {
            englishLabel.setForeground(new Color(204,204,204));
            languagesPanel.setVisible(false);
            languagesPanel.setVisible(true);
        }
    }

    private void nederlandsLabelMouseEntered(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==nederlandsLabel) {
            nederlandsLabel.setForeground(new Color(255,255,255));
            languagesPanel.setVisible(false);
            languagesPanel.setVisible(true);
        }
    }

    private void nederlandsLabelMouseExited(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==nederlandsLabel) {
            nederlandsLabel.setForeground(new Color(204,204,204));
            languagesPanel.setVisible(false);
            languagesPanel.setVisible(true);
        }
    }

    private void nederlandsLabelMousePressed(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==nederlandsLabel) {
            indexLanguage = 0;
            updateLabels();
            loginPanel.setVisible(false);
            loginPanel.setVisible(true);
            Thread th2 = new Thread() {
                @Override
                public void run() {
                try {
                    for ( int i = 1025; i < 1375; i++){
                        Thread.sleep(1);
                        languagesPanel.setLocation(i, 100);
                    }
                } catch (Exception e){
                    JOptionPane.showMessageDialog(null,e);
                }
            }
        };th2.start();
        languageValue = 0;
        }
    }

    private void englishLabelMousePressed(java.awt.event.MouseEvent evt) {
        if (evt.getSource()==englishLabel) {
            indexLanguage = 1;
            updateLabels();
            loginPanel.setVisible(false);
            loginPanel.setVisible(true);
            Thread th2 = new Thread() {
                @Override
                public void run() {
                try {
                    for ( int i = 1025; i < 1375; i++){
                        Thread.sleep(1);
                        languagesPanel.setLocation(i, 100);
                    }
                } catch (Exception e){
                    JOptionPane.showMessageDialog(null,e);
                }
            }
        };th2.start();
        languageValue = 0;
        }
    }

    private void updateLabels()
    {
        if(indexLanguage == 0)
        {
            Locale.setDefault(new Locale("NL"));
            languagesBundle = ResourceBundle.getBundle("LanguagesBundle");
        }
        else if(indexLanguage == 1)
        {
            Locale.setDefault(new Locale("EN"));
            languagesBundle = ResourceBundle.getBundle("LanguagesBundle");
        }
        busButton.setText(languagesBundle.getString("busButton"));
        metroButton.setText(languagesBundle.getString("metroButton"));
        tramButton.setText(languagesBundle.getString("tramButton"));
        treinButton.setText(languagesBundle.getString("treinButton"));
        vertrekLabel.setText("<html>" + languagesBundle.getString("vertrekLabel") + "</html>");
        vertrekpuntLabel.setText(languagesBundle.getString("vertrekpuntLabel"));
        datumLabel.setText(languagesBundle.getString("datumLabel"));
        bevestigingButton.setText(languagesBundle.getString("bevestigingButton"));
        trajectopslaanButton.setText(languagesBundle.getString("trajectopslaanButton"));
        favorietTrajectLabel.setText(languagesBundle.getString("favorietTrajectLabel"));
        tijdstipLabel.setText(languagesBundle.getString("tijdstipLabel"));
        aankomstLabel.setText(languagesBundle.getString("aankomstLabel"));
        aankomstpuntLabel.setText(languagesBundle.getString("aankomstpuntLabel"));
        signInLabel2.setText(languagesBundle.getString("signInLabel2"));
        usernameLabel1.setText(languagesBundle.getString("usernameLabel1"));
        passwordLabel.setText(languagesBundle.getString("passwordLabel"));
        loginButton.setText(languagesBundle.getString("loginButton"));
        registerLoginButton.setText(languagesBundle.getString("registerLoginButton"));
        stayLoggedInLabel.setText(languagesBundle.getString("stayLoggedInLabel"));
        forgotPasswordLabel.setText(languagesBundle.getString("forgotPasswordLabel"));
        messageLabel.setText(languagesBundle.getString("messageLabel"));
        messageLabel2.setText(languagesBundle.getString("messageLabel2"));
        //Ik heb liever dat Nederlands, Nederlands en English Engels blijft hetzelfde blijft, zodat ze het makkelijker kunnen vinden.
        languageLabel.setText(languagesBundle.getString("languageLabel"));
        nederlandsLabel.setText(languagesBundle.getString("nederlandsLabel"));
        englishLabel.setText(languagesBundle.getString("englishLabel"));
        //tot hier
        uwProfielLabel.setText(languagesBundle.getString("uwProfielLabel"));
        logUitButton.setText(languagesBundle.getString("logUitButton"));
        wijzigenButton.setText(languagesBundle.getString("wijzigenButton"));
        usernameProfielLabel.setText(languagesBundle.getString("usernameProfielLabel"));
        stationProfielLabel.setText(languagesBundle.getString("stationProfielLabel"));
        terugButton.setText(languagesBundle.getString("terugButton"));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                languagesBundle.getString("defaultTableModelVerterkStation"),
                languagesBundle.getString("defaultTableModelPerron"),
                languagesBundle.getString("defaultTableModelVertrekTijd"),
                languagesBundle.getString("defaultTableModelAankomstStation"),
                languagesBundle.getString("defaultTableModelAankomsttijd"),
                languagesBundle.getString("defaultTableModelAfstand")
            }
        ));
        forgotPasswordLabel.setToolTipText(languagesBundle.getString("forgotPasswordLabelToolTip"));
        uwProfielWijzigLabel.setText(languagesBundle.getString("uwProfielWijzigLabel"));
        usernameProfielWijzigLabel.setText(languagesBundle.getString("usernameProfielWijzigLabel"));
        stationWijzigLabel.setText(languagesBundle.getString("stationWijzigLabel"));
        annulerenButton.setText(languagesBundle.getString("annulerenButton"));
        opslaanButton.setText(languagesBundle.getString("opslaanButton"));
        registerLabel.setText(languagesBundle.getString("registerLabel"));
        usernameRegisterLabel.setText(languagesBundle.getString("usernameRegisterLabel"));
        stationRegisterLabel.setText(languagesBundle.getString("stationRegisterLabel"));
        cancelRegisterButton.setText(languagesBundle.getString("cancelRegisterButton"));
        registerRegisterButton.setText(languagesBundle.getString("registerRegisterButton"));
        passwordRegisterLabel.setText(languagesBundle.getString("passwordRegisterLabel"));
        retypePasswordRegisterLabel.setText(languagesBundle.getString("retypePasswordRegisterLabel"));
        messageLabel.setText(languagesBundle.getString("messageLabel"));
        messageLabel2.setText(languagesBundle.getString("messageLabel2"));
        messageLabel.setText(languagesBundle.getString("messageLabelRegister"));
        messageLabel2.setText(languagesBundle.getString("messageLabelRegister2"));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        if (args.length ==1 && args[0].equals("-runtester"))
        {
            Tester tester = new Tester();
            System.out.println("\n[!] Overall test result: " + tester.runTester() + "\n");
        }
        else
        {
            try {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (ClassNotFoundException ex) {
                java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                    new MainFrame().setVisible(true);
                }
            });
        }
    }

    private javax.swing.JLabel OVLogo;
    private javax.swing.JLabel OVLogo1;
    private javax.swing.JLabel OVLogo2;
    private javax.swing.JLabel OVLogo3;
    private javax.swing.JLabel OVLogo4;
    private javax.swing.JTextField aankomstText;
    private javax.swing.JLabel aankomstpuntLabel;
    private javax.swing.JButton annulerenButton;
    private javax.swing.JLabel backgroundLogin;
    private javax.swing.JLabel backgroundProfiel;
    private javax.swing.JLabel backgroundRegister;
    private javax.swing.JLabel backgroundWijzig;
    private javax.swing.JButton bevestigingButton;
    private javax.swing.JButton detailButton;
    private javax.swing.JPanel detailPanel;
    private javax.swing.JLabel detailLabel;
    private javax.swing.JLabel detailLabelTussenstop;
    private javax.swing.JLabel detailLabelVoorziening;
    private javax.swing.JLabel detailLabelVoorziening1;
    private javax.swing.JLabel detailLabelVoorziening2;
    private javax.swing.JLabel detailLabelVoorziening3;
    private javax.swing.JPanel detailknopPanel;
    private javax.swing.JButton trajectopslaanButton;
    private javax.swing.JLabel busButton;
    private javax.swing.JButton cancelRegisterButton;
    private javax.swing.JLabel closeButton;
    private javax.swing.JLabel closeButton1;
    private javax.swing.JLabel closeButton2;
    private javax.swing.JLabel closeButton3;
    private javax.swing.JLabel closeButton4;
    private javax.swing.JButton favorietTrajectButton;
    private javax.swing.JLabel datumLabel;
    private javax.swing.JTextField datumText;
    private javax.swing.JLabel dragbar;
    private javax.swing.JLabel dragbar1;
    private javax.swing.JLabel dragbar2;
    private javax.swing.JLabel dragbar3;
    private javax.swing.JLabel dragbar4;
    private javax.swing.JPanel favorietTrajectPanel;
    private javax.swing.JPanel favorieteTrajectenPanel;
    private javax.swing.JLabel favorietTrajectLabel;
    private javax.swing.JLabel englishLabel;
    private javax.swing.JLabel forgotPasswordLabel;
    private javax.swing.JPanel invoerPanel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel languageLabel;
    private javax.swing.JPanel languagesPanel;
    private javax.swing.JButton logUitButton;
    private javax.swing.JButton loginButton;
    private javax.swing.JPanel loginPanel;
    private javax.swing.JPanel loginTitleBarPanel;
    private javax.swing.JPanel loginTitleBarPanel1;
    private javax.swing.JPanel loginTitleBarPanel2;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JLabel messageLabel;
    private javax.swing.JLabel messageLabel2;
    private javax.swing.JLabel metroButton;
    private javax.swing.JLabel minButton;
    private javax.swing.JLabel minButton1;
    private javax.swing.JLabel minButton2;
    private javax.swing.JLabel minButton3;
    private javax.swing.JLabel minButton4;
    private javax.swing.JLabel nederlandsLabel;
    private javax.swing.JButton opslaanButton;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JLabel passwordRegisterLabel;
    private javax.swing.JPasswordField passwordRegisterText;
    private javax.swing.JPasswordField passwordText;
    private javax.swing.JPanel profielPanel;
    private javax.swing.JLabel registerLabel;
    private javax.swing.JButton registerLoginButton;
    private javax.swing.JPanel registerMainPanel;
    private javax.swing.JPanel registerPanel;
    private javax.swing.JButton registerRegisterButton;
    private javax.swing.JPanel registerTitalBarPanel;
    private javax.swing.JLabel retypePasswordRegisterLabel;
    private javax.swing.JPasswordField retypePasswordRegisterText;
    private javax.swing.JPanel selectBus;
    private javax.swing.JPanel selectMetro;
    private javax.swing.JPanel selectPanel;
    private javax.swing.JPanel selectTram;
    private javax.swing.JPanel selectTrein;
    private javax.swing.JLabel vertrekLabel;
    private javax.swing.JLabel aankomstLabel;
    private javax.swing.JLabel signInLabel2;
    private javax.swing.JPanel signInPanel;
    private javax.swing.JLabel stationProfielLabel;
    private javax.swing.JLabel stationProfielLabel2;
    private javax.swing.JLabel stationRegisterLabel;
    private javax.swing.JTextField stationRegisterText;
    private javax.swing.JLabel stationWijzigLabel;
    private javax.swing.JTextField stationWijzingText;
    private javax.swing.JCheckBox stayLoggedCheckBox;
    private javax.swing.JLabel stayLoggedInLabel;
    private javax.swing.JButton terugButton;
    private javax.swing.JLabel tijdstipLabel;
    private javax.swing.JTextField tijdstipText;
    private javax.swing.JLabel tramButton;
    private javax.swing.JLabel treinButton;
    private javax.swing.JLabel userIcon;
    private javax.swing.JLabel usernameLabel;
    private javax.swing.JLabel usernameLabel1;
    private javax.swing.JLabel usernameProfielLabel;
    private javax.swing.JLabel usernameProfielLabel2;
    private javax.swing.JLabel usernameProfielWijzigLabel;
    private javax.swing.JTextField usernameProfielWijzigText;
    private javax.swing.JLabel usernameRegisterLabel;
    private javax.swing.JTextField usernameRegisterText;
    private javax.swing.JTextField usernameText;
    private javax.swing.JLabel uwProfielLabel;
    private javax.swing.JPanel uwProfielPanel;
    private javax.swing.JPanel uwProfielPanel1;
    private javax.swing.JLabel uwProfielWijzigLabel;
    private javax.swing.JTextField vertrekText;
    private javax.swing.JLabel vertrekpuntLabel;
    private javax.swing.JPanel wijzigPanel;
    private javax.swing.JButton wijzigenButton;
    private javax.swing.JPanel kaartPanel;
    private javax.swing.JLabel kaartLabel;
    private javax.swing.JLabel trajectOpslaanLabel;
}