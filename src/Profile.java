import java.util.Date;
import java.time.LocalTime;
import java.util.ArrayList;

public class Profile
{
    private String userName;
    private String password;
    private String homeStation;
    private ArrayList<Integer> favorietTrajectBus = new ArrayList<Integer>();
    private ArrayList<LocalTime> favorietTrajectBusTijd = new ArrayList<LocalTime>();
    private ArrayList<Integer> favorietTrajectTram = new ArrayList<Integer>();
    private ArrayList<LocalTime> favorietTrajectTramTijd = new ArrayList<LocalTime>();
    private ArrayList<Integer> favorietTrajectTrein = new ArrayList<Integer>();
    private ArrayList<LocalTime> favorietTrajectTreinTijd = new ArrayList<LocalTime>();
    private ArrayList<Integer> favorietTrajectMetro = new ArrayList<Integer>();
    private ArrayList<LocalTime> favorietTrajectMetroTijd = new ArrayList<LocalTime>();
    private String detailTraject;

    public Profile()
    {
        this(null, null, null);
    }

    public Profile(String userName, String password, String homeStation)
    {
        this.userName = userName;
        this.password = password;
        this.homeStation = homeStation;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }

    public void setHomeStation(String homeStation)
    {
        this.homeStation = homeStation;
    }

    public String getHomeStation()
    {
        return homeStation;
    }

    public void setFavorietTrajectBus(int traject, LocalTime tijd)
    {
        favorietTrajectBus.add(traject);
        favorietTrajectBusTijd.add(tijd);
    }

    public void removeFavorietTrajectBus(int lijstNummer)
    {
        favorietTrajectBus.remove(lijstNummer);
        favorietTrajectBusTijd.remove(lijstNummer);
    }

    public ArrayList<Integer> getFavorietTrajectBus()
    {
        return favorietTrajectBus;
    }

    public ArrayList<LocalTime> getFavorietTrajectBusTijd()
    {
        return favorietTrajectBusTijd;
    }

    public void setFavorietTrajectTram(int traject, LocalTime tijd)
    {
        favorietTrajectTram.add(traject);
        favorietTrajectTramTijd.add(tijd);
    }

    public void removeFavorietTrajectTram(int lijstNummer)
    {
        favorietTrajectTram.remove(lijstNummer);
        favorietTrajectTramTijd.remove(lijstNummer);
    }

    public ArrayList<Integer> getFavorietTrajectTram()
    {
        return favorietTrajectTram;
    }

    public ArrayList<LocalTime> getFavorietTrajectTramTijd()
    {
        return favorietTrajectTramTijd;
    }

    public void setFavorietTrajectTrein(int traject, LocalTime tijd)
    {
        favorietTrajectTrein.add(traject);
        favorietTrajectTreinTijd.add(tijd);
    }
    
    public void removeFavorietTrajectTrein(int lijstNummer)
    {
        favorietTrajectTrein.remove(lijstNummer);
        favorietTrajectTreinTijd.remove(lijstNummer);
    }

    public ArrayList<Integer> getFavorietTrajectTrein()
    {
        return favorietTrajectTrein;
    }

    public ArrayList<LocalTime> getFavorietTrajectTreinTijd()
    {
        return favorietTrajectTreinTijd;
    }

    public void setFavorietTrajectMetro(int traject, LocalTime tijd)
    {
        favorietTrajectMetro.add(traject);
        favorietTrajectMetroTijd.add(tijd);
    }

    public void removeFavorietTrajectMetro(int lijstNummer)
    {
        favorietTrajectMetro.remove(lijstNummer);
        favorietTrajectMetroTijd.remove(lijstNummer);
    }

    public ArrayList<Integer> getFavorietTrajectMetro()
    {
        return favorietTrajectMetro;
    }

    public ArrayList<LocalTime> getFavorietTrajectMetroTijd()
    {
        return favorietTrajectMetroTijd;
    }

    public void setDetailTraject(String detailTraject)
    {
        this.detailTraject = detailTraject;
    }

    public String getDetailTraject(){return detailTraject;}

}
