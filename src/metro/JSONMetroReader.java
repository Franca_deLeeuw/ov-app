package metro;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;

public class JSONMetroReader
{
    public static ArrayList<MetroTijd> getMetroTijd()
    {
        JSONParser parser = new JSONParser();

        ArrayList<MetroTijd> metroList = new ArrayList<>();

        try
        {
            JSONArray array = (JSONArray) parser.parse(new FileReader("src/database/DatabaseMetro.json"));

            for (Object object : array)
            {
                ArrayList<LocalTime> reisTijden = new ArrayList<>();
                reisTijden.clear();

                JSONObject metroObject = (JSONObject) object;

                String stationVertrek = (String) metroObject.get("Vertrekstation");

                String stationAankomst = (String) metroObject.get("Aankomststation");

                String plaatsNaam = (String) metroObject.get("Plaatsnaam");

                String metroHalte = (String) metroObject.get("Metro halte");

                String traject = (String) metroObject.get("Traject");

                String reisTijd = (String) metroObject.get("Reistijd");

                String reisAfstand = (String) metroObject.get("Reisafstand");

                String metroNummer = (String) metroObject.get("Metro nummer");

                String tussenStop = (String) metroObject.get("Tussen stop");

                String eindPunt = (String) metroObject.get("Eind punt");

                String vertrekTijden = (String) metroObject.get("Vertrektijd");
                String aankomstTijden = (String) metroObject.get("Aankomsttijd");

                String vertrekTijden1 = (String)metroObject.get("Vertrektijd1");
                String aankomstTijden1 = (String) metroObject.get("Aankomsttijd1");

                String vertrekTijden2 = (String)metroObject.get("Vertrektijd2");
                String aankomstTijden2 = (String) metroObject.get("Aankomsttijd2");

                String vertrekTijden3 = (String)metroObject.get("Vertrektijd3");
                String aankomstTijden3 = (String) metroObject.get("Aankomsttijd3");

                String vertrekTijden4 = (String)metroObject.get("Vertrektijd4");
                String aankomstTijden4 = (String) metroObject.get("Aankomsttijd4");

                String vertrekTijden5 = (String)metroObject.get("Vertrektijd5");
                String aankomstTijden5 = (String) metroObject.get("Aankomsttijd5");

                reisTijden.add(LocalTime.parse(vertrekTijden));
                reisTijden.add(LocalTime.parse(aankomstTijden));
                reisTijden.add(LocalTime.parse(vertrekTijden1));
                reisTijden.add(LocalTime.parse(aankomstTijden1));
                reisTijden.add(LocalTime.parse(vertrekTijden2));
                reisTijden.add(LocalTime.parse(aankomstTijden2));
                reisTijden.add(LocalTime.parse(vertrekTijden3));
                reisTijden.add(LocalTime.parse(aankomstTijden3));
                reisTijden.add(LocalTime.parse(vertrekTijden4));
                reisTijden.add(LocalTime.parse(aankomstTijden4));
                reisTijden.add(LocalTime.parse(vertrekTijden5));
                reisTijden.add(LocalTime.parse(aankomstTijden5));

                MetroTijd metroTijd = new MetroTijd(stationVertrek, stationAankomst, plaatsNaam, metroHalte, traject,
                        reisTijd, reisAfstand, metroNummer, tussenStop,eindPunt, reisTijden);
                metroList.add(metroTijd);
            }

        }
        catch (ParseException | IOException e)
        {
            e.printStackTrace();
        }
        return metroList;
    }
}
