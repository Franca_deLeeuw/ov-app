package metro;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Arrays;

public class MetroTijd
{
	private String stationVertrek;
	private String stationAankomst;
	private String plaatsNaam;
	private String metroHalte;
	private String traject;
	private String metroNummer;
	private String reisTijd;
	private String reisAfstand;
	private	String tussenStop;
	private String eindPunt;
	private ArrayList<LocalTime> reisTijden;

	public MetroTijd(String stationVertrek, String stationAankomst, String plaatsNaam, String metroHalte, String traject,
					String metroNummer, String reisTijd, String reisAfstand, String tussenStop, String eindPunt,  ArrayList reisTijden)
	{
		this.stationVertrek = stationVertrek;
		this.stationAankomst = stationAankomst;
		this.plaatsNaam = plaatsNaam;
		this.metroHalte = metroHalte;
		this.traject = traject;
		this.metroNummer = metroNummer;
		this.reisTijd = reisTijd;
		this.reisAfstand = reisAfstand;
		this.tussenStop = tussenStop;
		this.eindPunt = eindPunt;
		this.reisTijden = reisTijden;
	}

	public String getStationVertrek()
	{
		return stationVertrek;
	}

	public String getStationAankomst()
	{
		return stationAankomst;
	}
	
	public String getPlaatsNaam()
	{
		return plaatsNaam;
	}

	public String getMetroHalte()
	{
		return metroHalte;
	}

	public String getTraject()
	{
		return traject;
	}

	public String getMetroNummer()
	{
		return metroNummer;
	}

	public String getReisTijd()
	{
		return reisTijd;
	}

	public String getReisAfstand()
	{
		return reisAfstand;
	}

	public String getTussenStop()
	{
		return tussenStop;
	}

	public ArrayList<LocalTime> getReisTijden()
	{
		return reisTijden;
	}

	public String getEindPunt(){return eindPunt;}
}
