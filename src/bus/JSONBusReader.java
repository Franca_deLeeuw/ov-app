package bus;

import bus.BusTijd;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import tram.TramTijd;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;


public class JSONBusReader
{
    public static ArrayList<BusTijd> getBusTijd()
    {
        JSONParser parser = new JSONParser();
        ArrayList<BusTijd> busList = new ArrayList<>();

        try
        {
            JSONArray array = (JSONArray) parser.parse(new FileReader("src/Database/DatabaseBus.json"));

            for (Object object : array)
            {
                ArrayList<LocalTime> reisTijden = new ArrayList<>();
                reisTijden.clear();

                JSONObject busObject = (JSONObject) object;

                String stationVertrek = (String) busObject.get("Vertrekstation");

                String stationAankomst = (String) busObject.get("Aankomststation");

                String plaatsNaam = (String) busObject.get("Plaatsnaam");

                String busHalte = (String) busObject.get("Bus halte");

                String traject = (String) busObject.get("Traject");

                String reisTijd = (String) busObject.get("Reistijd");

                String reisAfstand = (String) busObject.get("Reisafstand");

                String busNummer = (String) busObject.get("Bus nummer");

                String typeBus = (String) busObject.get("Type bus");

                String tussenStop = (String) busObject.get("Tussen stop");

                String eindPunt = (String) busObject.get("Eind punt");

                String vertrekTijden = (String) busObject.get("Vertrektijd");
                String aankomstTijden = (String) busObject.get("Aankomsttijd");

                String vertrekTijden1 = (String)busObject.get("Vertrektijd1");
                String aankomstTijden1 = (String) busObject.get("Aankomsttijd1");

                String vertrekTijden2 = (String)busObject.get("Vertrektijd2");
                String aankomstTijden2 = (String) busObject.get("Aankomsttijd2");

                String vertrekTijden3 = (String)busObject.get("Vertrektijd3");
                String aankomstTijden3 = (String) busObject.get("Aankomsttijd3");

                String vertrekTijden4 = (String)busObject.get("Vertrektijd4");
                String aankomstTijden4 = (String) busObject.get("Aankomsttijd4");

                String vertrekTijden5 = (String)busObject.get("Vertrektijd5");
                String aankomstTijden5 = (String) busObject.get("Aankomsttijd5");

                reisTijden.add(LocalTime.parse(vertrekTijden));
                reisTijden.add(LocalTime.parse(aankomstTijden));
                reisTijden.add(LocalTime.parse(vertrekTijden1));
                reisTijden.add(LocalTime.parse(aankomstTijden1));
                reisTijden.add(LocalTime.parse(vertrekTijden2));
                reisTijden.add(LocalTime.parse(aankomstTijden2));
                reisTijden.add(LocalTime.parse(vertrekTijden3));
                reisTijden.add(LocalTime.parse(aankomstTijden3));
                reisTijden.add(LocalTime.parse(vertrekTijden4));
                reisTijden.add(LocalTime.parse(aankomstTijden4));
                reisTijden.add(LocalTime.parse(vertrekTijden5));
                reisTijden.add(LocalTime.parse(aankomstTijden5));

                BusTijd busTijd = new BusTijd(stationVertrek, stationAankomst, plaatsNaam, busHalte, traject,
                        reisTijd, reisAfstand, busNummer, typeBus, tussenStop, eindPunt,reisTijden);

                busList.add(busTijd);
            }

            }
        catch (ParseException | IOException e)
        {
            e.printStackTrace();
        }
        return busList;
    }


}



