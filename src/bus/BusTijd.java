package bus;

import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.Object;
import java.time.LocalTime;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import com.fasterxml.jackson.databind.*;

public class BusTijd {
	private String stationVertrek;
	private String stationAankomst;
	private String plaatsNaam;
	private String busHalte;
	private String traject;
	private String busNummer;
	private String reisTijd;
	private String reisAfstand;
	private String tussenStop;
	private String typeBus;
	private String eindPunt;
	private ArrayList<LocalTime> reisTijden;

	// basis object aanmaken functie
	public BusTijd(String stationVertrek, String stationAankomst, String plaatsNaam, String bushalte, String traject,
				   String reisTijd, String reisAfstand, String busNummer, String typeBus, String tussenStop, String eindPunt, ArrayList reisTijden)
	{
		this.busHalte = bushalte;
		this.stationVertrek = stationVertrek;
		this.stationAankomst = stationAankomst;
		this.plaatsNaam = plaatsNaam;
		this.traject = traject;
		this.busNummer = busNummer;
		this.typeBus = typeBus;
		this.reisTijd = reisTijd;
		this.reisAfstand = reisAfstand;
		this.tussenStop = tussenStop;
		this.eindPunt = eindPunt;
		this.reisTijden = reisTijden;
	}

	public String getStationVertrek() {
		return stationVertrek;
	}

	public String getStationAankomst() {
		return stationAankomst;
	}

	public String getPlaatsNaam() {
		return plaatsNaam;
	}

	public String getBushalte() {
		return busHalte;
	}

	public String getTraject() {
		return traject;
	}

	public String getBusNummer() {
		return busNummer;
	}

	public String getTypeBus()
	{
		return typeBus;
	}

	public String getReisTijd() {
		return reisTijd;
	}

	public String getReisAfstand() {
		return reisAfstand;
	}

	public String getTussenStop() {
		return tussenStop;
	}

	public String getEindPunt()
	{
		return eindPunt;
	}

	public ArrayList<LocalTime> getReisTijden()
	{
		return reisTijden;
	}
}